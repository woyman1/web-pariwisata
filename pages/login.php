<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
    <title>Admin - <?= $pageName; ?></title>
   <!-- Bootstrap Core CSS -->
    <link href="admin/assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="admin/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="admin/assets/css/animate.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="admin/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!--My admin Custom CSS -->
    <link href="admin/plugins/bower_components/owl.carousel/owl.carousel.min.css" rel="stylesheet" type="text/css" />
    <link href="admin/plugins/bower_components/owl.carousel/owl.theme.default.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="admin/plugins/bower_components/datatables/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="admin/plugins/bower_components/html5-editor/bootstrap-wysihtml5.css" />
    <link href="admin/plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
    <link href="admin/plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="admin/assets/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="admin/assets/css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="admin/assets/css/colors/blue.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    <script src="http://www.w3schools.com/lib/w3data.js"></script>

</head>

<body>
    <!-- Preloader -->
    <!-- <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div> -->

  <div id="wrapper">
<?php

    // if ($_SESSION ['users_id'] AND $_SESSION['users_name'] AND $_SESSION['users_email'] ){
    //     header("location: index.php?page=dashboard");
    // }
    //     else {
    
    // }

?>

<section id="wrapper" style="background:url('../uploads/images/setting/<?= $login_banner?>') center center/cover no-repeat!important;height:100%;position:fixed" >
        <div class="login-box" style="margin-top: 5%;" >
            <div class="white-box" style="background: #E3E3E3" >
                <form class="form-horizontal form-material" id="loginform" action="auth/auth.php" method="post">
                    <center><img src="../uploads/images/setting/<?= $logo_text_dark; ?>" width="110" alt="home" />
                        <h2 class="box-title m-b-20 m-t-20">Log In as Admin</h2></center>
                    <div class="form-group ">
                        <div class="col-xs-12"> 
                            <input class="form-control" type="text" required placeholder="Username" name="username_login">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" required placeholder="Password" name="password_login">
                        </div>
                    </div> 
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="checkbox checkbox-primary pull-left p-t-0">
                                <input id="checkbox-signup" type="checkbox">
                                <label for="checkbox-signup"> Remember me </label>
                            </div>
                             </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit" name="login" >Log In</button>
                        </div>
                    </div>
                   
                </form>
                 
            </div>
        </div> 

    </section>
</div>
    <!-- jQuery -->
    <script src="admin/plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="admin/assets/bootstrap/dist/js/tether.min.js"></script>
    <script src="admin/assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="admin/plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="admin/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="admin/assets/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="admin/assets/js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="admin/assets/js/custom.min.js"></script>

    <script src="admin/plugins/bower_components/toast-master/js/jquery.toast.js"></script>
