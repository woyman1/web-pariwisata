<?php if(!$idGet){ header("location: index.php?page=404"); } ?>

	<section id="slideshow-destinations">
	<?php	
		$qSlide = "SELECT * FROM image i JOIN paket_tour pt ON i.travel_id = pt.id_paket WHERE i.travel_id = '$idGet' ";
		$getSlide = mysqli_query($connect, $qSlide);
		$slideAmount = mysqli_num_rows($getSlide);
		$amount = 1;
	?>

		<div id="carouselindicators" class="carousel slide" data-ride="carousel">
		  	<ol class="carousel-indicators">
			    <li data-target="#carouselindicators" data-slide-to="0" class="active"></li>
			     <?php

			  	for($a=1; $a<$slideAmount; $a++){ ?>
					<li data-target="#carouselindicators" data-slide-to="<?= $a ?>"></li>
			  
			  	<?php }?>
		  	</ol> 
		  	<div class="carousel-inner">
				
				<?php

				while($slide = mysqli_fetch_assoc($getSlide)){?>

					<div class="carousel-item <?php if($amount ==1) echo"active"; ?> ">
			      		<img class="d-block w-100" src="assets/img/dst/<?= $slide['img_name']; ?>" alt="First slide">
			      		<div class="carousel-caption d-none d-md-block">
			      			<div class="vl"></div>
			      			
				  		</div>
		    		</div>

			<?php $amount++;}?>

		    	
		  	</div>
		  
		  	<a class="carousel-control-prev" href="#carouselindicators" role="button" data-slide="prev">
		    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
		    <span class="sr-only">Previous</span>
		  </a>
		  <a class="carousel-control-next" href="#carouselindicators" role="button" data-slide="next">
		    <span class="carousel-control-next-icon" aria-hidden="true"></span>
		    <span class="sr-only">Next</span>
		  </a>
		</div>
	</section>

<?php
	
	$qDes = "SELECT * FROM paket_tour WHERE id_paket = '$idGet' ";
	$des = mysqli_query($connect, $qDes);
	$destination = mysqli_fetch_assoc($des);
	
	$description =  paragraf($destination['highlight']);
   	$descCounted = count($description);
   	$itenaery = paragraf($destination['itenaery']);
   	$iteCounted = count($itenaery);

   	// print_r($description);
 ?>

	<!-- SECTION TITLE
			================================================= -->
			<section id="title-page" class="full-width">
				<div class="container">
					<div class="row">
						<h3><?= $destination['paket_name']; ?>  </h3>
					</div>
				</div>
			</section>
			<!-- SECTION TITLE -->

	<section id="tab-menu">
		<div class="container">
			<div class="row" id="primary">
				<div id="content" class="col-md-20">
					<nav>
						<div class="nav nav-tabs" id="nav-tab" role="tablist">
						    <a class="nav-item nav-link active" id="nav-highlight-tab" data-toggle="tab" href="#nav-highlight" role="tab" aria-controls="nav-highlight" aria-selected="true">Highlight</a>
						    <a class="nav-item nav-link" id="nav-itenerary-tab" data-toggle="tab" href="#nav-itenerary" role="tab" aria-controls="nav-itenerary" aria-selected="false">Itenerary</a>
						    <a class="nav-item nav-link" id="nav-facilities-tab" data-toggle="tab" href="#nav-facilities" role="tab" aria-controls="nav-facilities" aria-selected="false">Facilities</a>
						    
						</div>
					</nav>
					<div class="tab-content" id="nav-tabContent">
					  	<div class="tab-pane fade show active" id="nav-highlight" role="tabpanel" aria-labelledby="nav-highlight-tab">

					  		<?php for($a = 0; $a<$descCounted; $a++){ ?>
                                    <p style="text-align: justify;"><?= $description[$a]; ?></p>
                                    <?php
                                        }
                                     ?>
					  	</div>
					  	<div class="tab-pane fade" id="nav-itenerary" role="tabpanel" aria-labelledby="nav-itenerary-tab">
						  	<?php for($a = 0; $a<$iteCounted; $a++){ ?>
                                    <p style="text-align: left;"><?= $itenaery[$a]; ?></p>
                                    <?php
                                        }
                                     ?>

						</div>
					  	<div class="tab-pane fade" id="nav-facilities" role="tabpanel" aria-labelledby="nav-facilities-tab">
						  	<?= $destination['facilities']; ?>
						</div>
					  	
				</div>				
			</div>
			<hr>
		</div>
	</section>

<!-- Travels Products -->
	<section id="travel-package">
	  	<div class="container">
	    	<div class="travels-listing spacer">
			    <div id="owl-example" class="owl-carousel">

			    	<?php 

			    	$qGetPaket = "SELECT * FROM paket_tour where id_paket != '$idGet' ";
			    	$getPaket = mysqli_query($connect, $qGetPaket);
			    	while($paket = mysqli_fetch_assoc($getPaket)){

			    	?>
			          	<div class="travels">
				          	<div class="packagelink animation-element">
								<div class="package  border-radius-base box-shadow-hover">
									<div class="card">
										<img src="assets/img/dst/<?= $paket['cover_img']; ?>" class="img-responsive" alt="package">
										<div class="package-body">
											<div class="package-title"><h5 ><?= $paket['paket_name']; ?></h5></div>
											<div class="package-text" style="margin-bottom: 20px;">
												<p style="text-align: justify; margin-top: 0px;"> 
													<?= batasiKalimat100($paket['highlight']); ?>
													<a href="packages&id=<?= $paket['id_paket']; ?>">See more</a>
												</p>

												<span class="text-danger" style="font-weight: bold;">
													<?= $paket['usd_price'].' / '.$paket['idr_price'] ?>
												</span>
											</div>
										</div>
										<div class="package-footer ">
											<a class="btn btn-primary btn-book btn-xs btn-block" href="content-packages-page.html">Book Now</a>
										</div>
									</div>
								</div>
							</div>
						</div>

					<?php
						}
					 ?>
			          	
			    </div>
	    	</div>
	  	</div>
	</section>
	<!-- End Travels Products -->
