	<!-- Image Slideshow -->
	<section >

		<?php

		$qSlide = "SELECT * FROM image i JOIN destination d ON i.dest_id = d.dst_id WHERE i.slideshow = 1 ";
		$getSlide = mysqli_query($connect, $qSlide);
		$slideAmount = mysqli_num_rows($getSlide);
		$amount = 1;

		 ?>

	    <div id="slider" class="sl-slider-wrapper">

	        <div class="sl-slider"> 
	          
	         <?php  
	         	while($slide = mysqli_fetch_assoc($getSlide)){
	         ?>
	         	
	          <div class="sl-slide" data-orientation="vertical" data-slice1-rotation="10" data-slice2-rotation="-15" data-slice1-scale="1.5" data-slice2-scale="1.5">
	            <div class="sl-slide-inner">
	              <div class="bg-img bg-img-2" style="background-image: url(assets/img/dst/<?= $slide['img_name']; ?>)"></div>
	              <h2><a href="#"><?= $slide['dst_name'] ?></a></h2>
	              <blockquote>              
	              <p class="location"><span class="glyphicon glyphicon-map-marker"></span> <?= $slide['dst_location']; ?></p>
	              <p><?= $slide['dst_desc_singkat']; ?></p>
	              <!-- <cite>$ 20,000,000</cite> -->
	              </blockquote>
	            </div> 
	          </div>

	     	 <?php $amount++;  } ?>

	        </div><!-- /sl-slider -->

	        <nav id="nav-dots" class="nav-dots">
	        	<?php for($s=0; $s<$slideAmount; $s++){
	        		echo "<span></span>";	
	        	} ?>
	      		<span></span>
	        </nav>
	    </div><!-- /slider-wrapper --> 
	</section>
	<!-- End Image Slideshow -->

			<!-- SECTION TITLE
			================================================= -->
			<!-- <section id="title-page" class="full-width">
				<div class="container">
					<div class="row">
						<h3>Coming Soon</h3>
					</div>
				</div>
			</section> -->
			<!-- SECTION TITLE -->


	<!-- VIDEO FEATURETTE
	================================================== -->
	<!-- <section id="feature">
		<div class="container">
			<div class="row feature">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 feature-video offset-2">
					<iframe src="https://www.youtube.com/embed/i9MJwL06-Ic" frameborder="0" allowfullscreen></iframe>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 feature-event">
					<h4>Lorem ipsum dolor sit amet </h4>
					<ul>
						<li><a href=""> Lorem ipsum dolor sit amet, consectetur adipisicing elit,..</a></li>
						<li><a href=""> Lorem ipsum dolor sit amet, consectetur adipisicing elit,..</a></li>
						<li><a href=""> Lorem ipsum dolor sit amet,..</a></li>
						<li><a href=""> Lorem ipsum dolor sit amet, consectetur adipisicing elit,..</a></li>
						<li><a href=""> Lorem ipsum dolor sit amet, consectetur adipisicing elit,..</a></li>

					</ul>
					<div class="more">
						<a href="packages-page.html"> More Event...</a>
					</div>
				</div>
			</div>		
		</div>
	</section> -->
	<!-- End Featurette -->

			<!-- SECTION TITLE
			================================================= -->
			<section id="title-page" class="full-width">
				<div class="container">
					<div class="title" >
						<h3>Destination Highlight</h3>
					</div>
				</div>
			</section>
			<!-- SECTION TITLE -->

	<!-- DESTINATION HIGLIGHT
	================================================= -->
	<section id="dest-higlight">
		<div class="container">
			<div class="destinations-listing spacer">
						<div class="row destination-row clearfix card-group center">

						<?php 
							$qGetDestination = "SELECT * FROM destination ORDER BY dst_id DESC limit 8";
							$getDestination = mysqli_query($connect, $qGetDestination);
							while($desti = mysqli_fetch_assoc($getDestination)){
						 ?>	
							<a href="dest&id=<?= $desti['dst_id'] ?> " class="col-xs-12 col-sm-4 col-md-3 col-lg-3 destination animation-element ">
								<div class="thumbnail box-shadow-hover">
									<img src="assets/img/dst/<?= $desti['dst_image_cover']; ?>" alt="Lights">
									<div class="caption">
									   	<h5><?= $desti['dst_name'] ?></h5>
									      <p><?= $desti['dst_desc_singkat']; ?></p>
									</div>
								</div>
							</a>

						<?php
							}
						 ?>
							

						<div class="more">
					          	<a  href="destination">More Destinations...</a>
						</div>					    
			</div>
		</div>
	</section>
	<!-- End Destination -->


			<!-- SECTION TITLE
			================================================= -->
			<section id="title-page" class="full-width">
				<div class="container">
					<div class="title" > 
						<a href="packages-page.html" class="more pull-right viewall">View All Listing</a>
      					<h3>Travel Packages</h3>
      				</div>
				</div>
			</section>
			<!-- SECTION TITLE -->


	<!-- Travels Products -->
	<section id="travel-package">
	  	<div class="container">
	    	<div class="travels-listing spacer">
			    <div id="owl-example" class="owl-carousel">

			    	<?php 

			    	$qGetPaket = "SELECT * FROM paket_tour";
			    	$getPaket = mysqli_query($connect, $qGetPaket);
			    	while($paket = mysqli_fetch_assoc($getPaket)){

			    	?>
			          	<div class="travels">
				          	<div class="packagelink animation-element">
								<div class="package  border-radius-base box-shadow-hover">
									<div class="card">
										<img src="assets/img/dst/<?= $paket['cover_img']; ?>" class="img-responsive" alt="package">
										<div class="package-body">
											<div class="package-title"><h5 ><?= $paket['paket_name']; ?></h5></div>
											<div class="package-text" style="margin-bottom: 20px;">
												<p style="text-align: justify; margin-top: 0px;"> 
													<?= batasiKalimat100($paket['highlight']); ?>
													<a href="packages&id=<?= $paket['id_paket']; ?>">See more</a>
												</p>

												<span class="text-danger" style="font-weight: bold;">
													<?= $paket['usd_price'].' / '.$paket['idr_price'] ?>
												</span>
											</div>
										</div>
										<div class="package-footer ">
											<a class="btn btn-primary btn-book btn-xs btn-block" href="content-packages-page.html">Book Now</a>
										</div>
									</div>
								</div>
							</div>
						</div>

					<?php
						}
					 ?>
			          	
			    </div>
	    	</div>
	  	</div>
	</section>
	<!-- End Travels Products -->



