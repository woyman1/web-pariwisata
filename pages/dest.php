<?php if(!$idGet){ header("location: index.php?page=404"); } ?>

	<section id="slideshow-destinations">
	<?php	
		$qSlide = "SELECT * FROM image i JOIN destination d ON i.dest_id = d.dst_id WHERE i.dest_id = '$idGet' ";
		$getSlide = mysqli_query($connect, $qSlide);
		$slideAmount = mysqli_num_rows($getSlide);
		$amount = 1;
	?>

		<div id="carouselindicators" class="carousel slide" data-ride="carousel">
		  	<ol class="carousel-indicators">
			    <li data-target="#carouselindicators" data-slide-to="0" class="active"></li>
			     <?php

			  	for($a=1; $a<$slideAmount; $a++){ ?>
					<li data-target="#carouselindicators" data-slide-to="<?= $a ?>"></li> 
			  
			  	<?php }?>
		  	</ol> 
		  	<div class="carousel-inner">
				
				<?php

				while($slide = mysqli_fetch_assoc($getSlide)){?>

					<div class="carousel-item <?php if($amount ==1) echo"active"; ?> ">
			      		<img class="d-block w-100" src="assets/img/dst/<?= $slide['img_name']; ?>" alt="First slide">
			      		<div class="carousel-caption d-none d-md-block">
			      			<div class="vl"></div>
			      			<div class="text-caption-header" style="background: rgba(0,0,0, 0.5); padding: 10px;">
				    			<h5><?= $slide['dst_name'] ?></h5>
				    			<p><?= $slide['dst_desc_singkat']; ?></p>
				  			</div>
				  		</div>
		    		</div>

			<?php $amount++;}?>

		    	
		  	</div>
		  
		  	<a class="carousel-control-prev" href="#carouselindicators" role="button" data-slide="prev">
		    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
		    <span class="sr-only">Previous</span>
		  </a>
		  <a class="carousel-control-next" href="#carouselindicators" role="button" data-slide="next">
		    <span class="carousel-control-next-icon" aria-hidden="true"></span>
		    <span class="sr-only">Next</span>
		  </a>
		</div>
	</section>

<?php
	
	$qDes = "SELECT * FROM destination WHERE dst_id = '$idGet' ";
	$des = mysqli_query($connect, $qDes);
	$destination = mysqli_fetch_assoc($des);
	
	$description =  paragraf($destination['dst_desc']);
   	$descCounted = count($description);

   	// print_r($description);
 ?>

	<!-- SECTION TITLE
			================================================= -->
			<section id="title-page" class="full-width">
				<div class="container">
					<div class="row">
						<h3><?= $destination['dst_name']; ?></h3>
					</div>
				</div>
			</section>
			<!-- SECTION TITLE -->

	<section id="dest-description">
		<div class="container">
			<div class="row" id="primary">
				<div id="content">
					<div class="row clearfix center">
						<div class="content-description col-xs-12 col-sm-12 col-md-7 col-lg-8">
							<div>
								<h3><?= $destination['dst_name']; ?></h3>
								  <?php for($a = 0; $a<$descCounted; $a++){ ?>
                                    <p style="text-align: justify;"><?= $description[$a]; ?></p>
                                    <?php
                                        }
                                     ?>
							</div>
							<!-- MAP -->
	                        <div>
	                            <h4><span class="glyphicon glyphicon-map-marker"></span> <?= $destination['dst_location'] ?></h4>
	                            <div class="well"><iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3953.288442946947!2d110.40780211593751!3d-7.759202631304444!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a599bd3bdc4ef%3A0x6f1714b0c4544586!2sUniversitas+Amikom+Yogyakarta!5e0!3m2!1sid!2sid!4v1530812442548"></iframe></div>
	                        </div> 
	                        <!-- END MAP -->							
						</div>

						<div class="content-higlight col-xs-12 col-sm-6 col-md-5 col-lg-4">
							<div class="row dest-tumbnail">
								<?php 

								$qDesList = "SELECT * FROM destination WHERE dst_id != '$idGet' ORDER BY dst_date_post DESC limit 6";
								$getDestList = mysqli_query($connect, $qDesList);
								while($destList = mysqli_fetch_assoc($getDestList)){
								?>
								<div class="tile-highlight">
									<a class="tile-text" href="index.php?page=dest&id=<?= $destList['dst_id']; ?>">
										<h4><?= $destList['dst_name'] ?></h4>
										<p><?= batasiKalimat100($destList['dst_desc_singkat']) ?></p>
									</a>
								</div>
								<?php 
									}
								?>

								<div class="more" style="padding-left: 250px;padding-right: 10px;">
									<p><a href="destination"> More Destinations...</a></p>
								</div>
							</div>
						</div>
					</div>
					<hr>
				</div>

			</div>
		</div>
	</section>

	<!-- Travels Products -->
	<section id="travel-package">
	  	<div class="container">
	    	<div class="travels-listing spacer">
			    <div id="owl-example" class="owl-carousel">
			          	<div class="travels">
				          	<div class="packagelink animation-element">
								<div class="package  border-radius-base box-shadow-hover">
									<div class="card">
										<img src="assets/img/001.jpg" class="img-responsive" alt="package">
										<div class="package-body">
											<div class="package-title"><h5 >PackageTitle</h5></div>
											<div class="package-text">
												<ul>
													<li>This is a description</li>
													<li>This is a description</li>
													<li>This is a description</li>
													<li>This is a description</li>
												</ul>
											</div>
										</div>
										<div class="package-footer ">
											<a class="btn btn-primary btn-book btn-xs btn-block" href="content-packages-page.html">Book Now</a>
										</div>
									</div>
								</div>
							</div>
						</div>
			          	<div class="travels">
				          	<div class="packagelink animation-element">
								<div class="package  border-radius-base box-shadow-hover">
									<div class="card">
										<img src="assets/img/001.jpg" class="img-responsive" alt="package">
										<div class="package-body">
											<div class="package-title"><h5 >PackageTitle</h5></div>
											<div class="package-text">
												<ul>
													<li>This is a description</li>
													<li>This is a description</li>
													<li>This is a description</li>
													<li>This is a description</li>
												</ul>
											</div>
										</div>
										<div class="package-footer ">
											<a class="btn btn-primary btn-book btn-xs btn-block" href="content-packages-page.html">Book Now</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="travels">
				          	<div class="packagelink animation-element">
								<div class="package  border-radius-base box-shadow-hover">
									<div class="card">
										<img src="assets/img/001.jpg" class="img-responsive" alt="package">
										<div class="package-body">
											<div class="package-title"><h5 >PackageTitle</h5></div>
											<div class="package-text">
												<ul>
													<li>This is a description</li>
													<li>This is a description</li>
													<li>This is a description</li>
													<li>This is a description</li>
												</ul>
											</div>
										</div>
										<div class="package-footer ">
											<a class="btn btn-primary btn-book btn-xs btn-block" href="content-packages-page.html">Book Now</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="travels">
				          	<div class="packagelink animation-element">
								<div class="package  border-radius-base box-shadow-hover">
									<div class="card">
										<img src="assets/img/001.jpg" class="img-responsive" alt="package">
										<div class="package-body">
											<div class="package-title"><h5 >PackageTitle</h5></div>
											<div class="package-text">
												<ul>
													<li>This is a description</li>
													<li>This is a description</li>
													<li>This is a description</li>
													<li>This is a description</li>
												</ul>
											</div>
										</div>
										<div class="package-footer ">
											<a class="btn btn-primary btn-book btn-xs btn-block" href="content-packages-page.html">Book Now</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="travels">
				          	<div class="packagelink animation-element">
								<div class="package  border-radius-base box-shadow-hover">
									<div class="card">
										<img src="assets/img/001.jpg" class="img-responsive" alt="package">
										<div class="package-body">
											<div class="package-title"><h5 >PackageTitle</h5></div>
											<div class="package-text">
												<ul>
													<li>This is a description</li>
													<li>This is a description</li>
													<li>This is a description</li>
													<li>This is a description</li>
												</ul>
											</div>
										</div>
										<div class="package-footer ">
											<a class="btn btn-primary btn-book btn-xs btn-block" href="content-packages-page.html">Book Now</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="travels">
				          	<div class="packagelink animation-element">
								<div class="package  border-radius-base box-shadow-hover">
									<div class="card">
										<img src="assets/img/001.jpg" class="img-responsive" alt="package">
										<div class="package-body">
											<div class="package-title"><h5 >PackageTitle</h5></div>
											<div class="package-text">
												<ul>
													<li>This is a description</li>
													<li>This is a description</li>
													<li>This is a description</li>
													<li>This is a description</li>
												</ul>
											</div>
										</div>
										<div class="package-footer ">
											<a class="btn btn-primary btn-book btn-xs btn-block" href="content-packages-page.html">Book Now</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="travels">
				          	<div class="packagelink animation-element">
								<div class="package  border-radius-base box-shadow-hover">
									<div class="card">
										<img src="assets/img/001.jpg" class="img-responsive" alt="package">
										<div class="package-body">
											<div class="package-title"><h5 >PackageTitle</h5></div>
											<div class="package-text">
												<ul>
													<li>This is a description</li>
													<li>This is a description</li>
													<li>This is a description</li>
													<li>This is a description</li>
												</ul>
											</div>
										</div>
										<div class="package-footer ">
											<a class="btn btn-primary btn-book btn-xs btn-block" href="content-packages-page.html">Book Now</a>
										</div>
									</div>
								</div>
							</div>
						</div>
			    </div>
	    	</div>
	  	</div>
	</section>
	<!-- End Travels Products -->
