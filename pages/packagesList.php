	
	<section id="slideshow-destinations" style="max-height: 1000px; height: 593px;">
		<?php

		$qSlide = "SELECT * FROM image i JOIN destination d ON i.dest_id = d.dst_id WHERE i.slideshow = 1 ";
		$getSlide = mysqli_query($connect, $qSlide);
		$slideAmount = mysqli_num_rows($getSlide);
		$amount = 1;

		 ?>

		<div id="carouselindicators" class="carousel slide" data-ride="carousel">
		  	<ol class="carousel-indicators">
			    <li data-target="#carouselindicators" data-slide-to="0" class="active"></li>
			  <?php

			  	for($a=1; $a<$slideAmount; $a++){ ?>
					<li data-target="#carouselindicators" data-slide-to="<?= $a ?>"></li>
			  
			  <?php }?>
			    
		  	</ol> 
		  	<div class="carousel-inner">
	
			<?php

				while($slide = mysqli_fetch_assoc($getSlide)){?>

					<div class="carousel-item <?php if($amount ==1) echo"active"; ?> ">
			      		<img class="d-block w-100" src="assets/img/dst/<?= $slide['img_name']; ?>" alt="First slide">
			      		<div class="carousel-caption d-none d-md-block">
			      			<div class="vl"></div>
			      			<div class="text-caption-header" style="background: rgba(0,0,0, 0.5); padding: 10px;">
				    			<h5><?= $slide['dst_name'] ?></h5>
				    			<p><?= $slide['dst_desc_singkat']; ?></p>
				  			</div>
				  		</div>
		    		</div>

			<?php $amount++;}?>

		    	
		    	
		  	</div>
		  
		  	<a class="carousel-control-prev" href="#carouselindicators" role="button" data-slide="prev">
		    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
		    <span class="sr-only">Previous</span>
		  </a>
		  <a class="carousel-control-next" href="#carouselindicators" role="button" data-slide="next">
		    <span class="carousel-control-next-icon" aria-hidden="true"></span>
		    <span class="sr-only">Next</span>
		  </a>
		</div>
	</section>

			<!-- SECTION TITLE
			================================================= -->
			<section id="title-page" class="full-width">
				<div class="container">
					<div class="row">
						<h3>Book Your Packages</h3>
					</div>
				</div>
			</section>
			<!-- SECTION TITLE -->

	<!-- Travels Products -->
	<section id="travel-package">
	  	<div class="container">
	    	<div class="travels-listing spacer">
			    <div id="owl-example" class="owl-carousel">

			    	<?php 

			    	$qGetPaket = "SELECT * FROM paket_tour";
			    	$getPaket = mysqli_query($connect, $qGetPaket);
			    	while($paket = mysqli_fetch_assoc($getPaket)){

			    	?>
			          	<div class="travels">
				          	<div class="packagelink animation-element">
								<div class="package  border-radius-base box-shadow-hover">
									<div class="card">
										<img src="assets/img/dst/<?= $paket['cover_img']; ?>" class="img-responsive" alt="package">
										<div class="package-body">
											<div class="package-title"><h5 ><?= $paket['paket_name']; ?></h5></div>
											<div class="package-text" style="margin-bottom: 20px;">
												<p style="text-align: justify; margin-top: 0px;"> 
													<?= batasiKalimat100($paket['highlight']); ?>
													<a href="packages&id=<?= $paket['id_paket']; ?>">See more</a>
												</p>

												<span class="text-danger" style="font-weight: bold;">
													<?= $paket['usd_price'].' / '.$paket['idr_price'] ?>
												</span>
											</div>
										</div>
										<div class="package-footer ">
											<a class="btn btn-primary btn-book btn-xs btn-block" href="content-packages-page.html">Book Now</a>
										</div>
									</div>
								</div>
							</div>
						</div>

					<?php
						}
					 ?>
			          	
			    </div>
	    	</div>
	  	</div>
	</section>
	<!-- End Travels Products -->