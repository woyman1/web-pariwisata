	
	<section id="slideshow-destinations" style="max-height: 1000px; height: 593px;">
		<?php

		$qSlide = "SELECT * FROM image i JOIN destination d ON i.dest_id = d.dst_id WHERE i.slideshow = 1 ";
		$getSlide = mysqli_query($connect, $qSlide);
		$slideAmount = mysqli_num_rows($getSlide);
		$amount = 1;

		 ?>

		<div id="carouselindicators" class="carousel slide" data-ride="carousel">
		  	<ol class="carousel-indicators">
			    <li data-target="#carouselindicators" data-slide-to="0" class="active"></li>
			  <?php

			  	for($a=1; $a<$slideAmount; $a++){ ?>
					<li data-target="#carouselindicators" data-slide-to="<?= $a ?>"></li>
			  
			  <?php }?>
			    
		  	</ol> 
		  	<div class="carousel-inner">
	
			<?php

				while($slide = mysqli_fetch_assoc($getSlide)){?>

					<div class="carousel-item <?php if($amount ==1) echo"active"; ?> ">
			      		<img class="d-block w-100" src="assets/img/dst/<?= $slide['img_name']; ?>" alt="First slide">
			      		<div class="carousel-caption d-none d-md-block">
			      			<div class="vl"></div>
			      			<div class="text-caption-header" style="background: rgba(0,0,0, 0.5); padding: 10px;">
				    			<h5><?= $slide['dst_name'] ?></h5>
				    			<p><?= $slide['dst_desc_singkat']; ?></p>
				  			</div>
				  		</div>
		    		</div>

			<?php $amount++;}?>

		    	
		    	
		  	</div>
		  
		  	<a class="carousel-control-prev" href="#carouselindicators" role="button" data-slide="prev">
		    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
		    <span class="sr-only">Previous</span>
		  </a>
		  <a class="carousel-control-next" href="#carouselindicators" role="button" data-slide="next">
		    <span class="carousel-control-next-icon" aria-hidden="true"></span>
		    <span class="sr-only">Next</span>
		  </a>
		</div>
	</section>

			<!-- SECTION TITLE
			================================================= -->
			<section id="title-page" class="full-width">
				<div class="container">
					<div class="row">
						<h3>Explore Your Destination</h3>
					</div>
				</div>
			</section>
			<!-- SECTION TITLE -->

	<!-- SECTION 2
	================================================= -->
	<section id="dest-higlight" style="margin-bottom: 50px; margin-top: 20px;">
		<div class="container">
				<div class="row destination-row clearfix card-group center">
	
					<?php 
							$qGetDestination = "SELECT * FROM destination ORDER BY dst_id DESC limit 8";
							$getDestination = mysqli_query($connect, $qGetDestination);
							while($desti = mysqli_fetch_assoc($getDestination)){
					?>	

							<a href="index.php?page=dest&id=<?= $desti['dst_id'] ?>" class="col-xs-12 col-sm-4 col-md-3 col-lg-3 destination animation-element ">
								<div class="thumbnail box-shadow-hover">
									<img src="assets/img/dst/<?= $desti['dst_image_cover']; ?>" alt="Lights">
									<div class="caption">
									   	<h5><?= $desti['dst_name'] ?></h5>
									      <p><?= $desti['dst_desc_singkat']; ?></p>
									</div>
								</div>
							</a>

						<?php
							}
						 ?>
							
				</div>				    
		</div>
	</section>
	<!-- SECTION 2 -->
	
