<div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse slimscrollsidebar">
                <ul class="nav" id="side-menu">
                    <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                        <!-- input-group -->
                        <div class="input-group custom-search-form"> 
                            <input type="text" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
            <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
            </span> </div>
                        <!-- /input-group -->
                    </li>

                    <li class="nav-small-cap m-t-30">--- Main Menu</li>
                    <li> <a href="index.php?page=dashboard" class="waves-effect active"><i class="fa fa-dashboard" ></i> <span class="hide-menu"> Dashboard </a>
                    </li>

                    <li> <a href="javascript:void(0)" class="waves-effect"><i class="ti-map-alt"></i> <span class="hide-menu">Destination<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="index.php?page=destinationList"><i class="fa fa-list"></i>&nbsp;List Destination</a> </li>
                       
                            <li> <a href="index.php?page=destinationAdd"><i class="fa fa-plus-square text-info"></i>&nbsp;Add New Destination</a> </li>                                                       
                        </ul>
                    </li>

                    <li> <a href="javascript:void(0)" class="waves-effect"><i class="ti-list"></i> <span class="hide-menu">Travel Package<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="index.php?page=travelList"><i class="fa fa-list"></i>&nbsp;List Travel Package</a> </li>
                            <li> <a href="index.php?page=travelAdd" class="waves-effect"><i class="fa fa-plus-square text-info"></i>&nbsp; Add New Travel Package </a> </li>
                                
                        </ul>
                    </li>

                

                    <!-- <li> <a href="javascript:void(0)" class="waves-effect"><i class="ti-user"></i> <span class="hide-menu">User<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">

                            <li> <a href="javascript:void(0)" class="waves-effect text-primary"><i style="color:CRIMSON" class="ti-user"></i>&nbsp; Admin <span class="fa arrow pull-right"></span></a>
                                <ul class="nav nav-third-level">
                                    <li> <a href="index.php?page=adminList">List Admin</a> </li>
                                    <li> <a href="index.php?page=adminAdd"><i class="fa fa-plus-square text-info"></i>&nbsp;Tambah Admin Baru</a> </li>
                                </ul>
                            </li>
 
                            <li> <a href="javascript:void(0)" class="waves-effect text-primary"><i style="color:ROYALBLUE" class="ti-user"></i>&nbsp; Member <span class="fa arrow pull-right"></span></a>
                                <ul class="nav nav-third-level">
                                    <li> <a href="index.php?page=memberList">List Member</a> </li>

                                    <li> <a href="index.php?page=memberAdd"><i class="fa fa-plus-square text-info"></i>&nbsp;Tambah Member Baru</a> </li>
                   
                                </ul>
                            </li>                            
                        </ul>
                    </li> -->

                    <li> <a href="javascript:void(0)" class="waves-effect"><i class="ti-settings"></i> <span class="hide-menu">Setting Site<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="index.php?page=generalSetting">General Setting</a> </li>
                            <li> <a href="index.php?page=settingLogo">Ganti Logo</a>
                            </li>
                        </ul>
                    </li>


                </ul>
            </div>
        </div>
