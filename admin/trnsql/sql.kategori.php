<?php

include ("../../config/db.connect.php");
include ("../../config/site.config.php");
include ("../../inc/inc.variable.php");


if (isset($_POST['create'])) :
  $namaKategori = filter_var($_POST['namaKategory'], FILTER_SANITIZE_STRING);

  if (isset($_FILES['imgKategory'])):
    $errors= array();
    $file_name = uniqid();
    $file_size =$_FILES['imgKategory']['size'];
    $file_tmp =$_FILES['imgKategory']['tmp_name'];
    $file_type=$_FILES['imgKategory']['type'];
    $file_ext=strtolower(end(explode('.',$_FILES['imgKategory']['name'])));
    $file_name2 = uniqid().".".$file_ext;
    $expensions= array("jpeg","jpg","png");

    if(in_array($file_ext,$expensions)=== false){
         $errors[]="extension not allowed, please choose a JPEG or PNG file.";
    }
    //
    // if($file_size > 2097152){
    //      $errors[]='File size must be excately 2 MB';
    // }

    if(empty($errors)==true){
        $query = "INSERT INTO zp_kategori(kategori_nama, kategori_icon) VALUES ('$namaKategori', '$file_name2')";
        $run = pg_query($connect, $query);

        if ($run) :
          move_uploaded_file($file_tmp,"../../upload/icon/kategory/".$file_name2);
          echo "Success";
        else:
          echo pg_last_error($connect);
        endif;


     }else{
        print_r($errors);
     }

     header("location: ../index.php?page=kategoriList");
  endif; //if there is an img
endif; // if there is a post

if($actionGet == 'delete') :
  $id = $_GET['id'];

  $q_fileName = "SELECT * FROM zp_kategori WHERE kategori_id = $id";
  $r_fileName = pg_query($q_fileName);
  $data_fileName = pg_fetch_assoc($r_fileName);

  if ($data_fileName['kategori_icon']) {
    unlink("../../upload/icon/kategory/".$data_fileName['kategori_icon']);
  }

  $query = "DELETE FROM zp_kategori WHERE kategori_id = $id";
  $run = pg_query($query);

  if (pg_affected_rows($run) > 0) {
    echo "Berhasil menghapus data";
  }else{
    echo "Gagal menghapus data";
  }

endif;// if delete record

if (isset($_POST['edit'])) :
  $kategori_nama = $_POST['namaKategory'];
  $file_path = "../".$_POST['file_path'];

  if (is_uploaded_file($_FILES['imgKategory']['tmp_name'])) :
    $errors= array();
    $file_name = uniqid();
    $file_size =$_FILES['imgKategory']['size'];
    $file_tmp =$_FILES['imgKategory']['tmp_name'];
    $file_type=$_FILES['imgKategory']['type'];
    $file_ext=strtolower(end(explode('.',$_FILES['imgKategory']['name'])));
    $file_name2 = uniqid().".".$file_ext;
    $expensions= array("jpeg","jpg","png");

    if(in_array($file_ext,$expensions)=== false):
         $errors[]="extension not allowed, please choose a JPEG or PNG file.";
    endif; // if ekstensi file tepat


    if(empty($errors)==true):
        $query = "UPDATE zp_kategori
                  SET kategori_nama = '$kategori_nama', kategori_icon = '$file_name2' 
                  WHERE kategori_id = $idPost";
        $run = pg_query($connect, $query);

        if ($run) :
          unlink($file_path);
          move_uploaded_file($file_tmp,"../../upload/icon/kategory/".$file_name2);
          echo "Success";
          header("location: ../index.php?page=kategoriList");
        else:
          echo pg_last_error($connect);
        endif;

     else:
        print_r($errors);
     endif; //end if tak ada error lalu upload

  else:
    $query = "UPDATE zp_kategori
              SET kategori_nama = '$kategori_nama'
              WHERE kategori_id = $idPost";
    $run = pg_query($connect, $query);

    if ($run) :
      echo "Success";
      header("location: ../index.php?page=kategoriList");
    else:
      echo pg_last_error($connect);
    endif;
    
  endif; //end if ganti ikon

endif; // if edit data ikon


 ?>
