<?php
include ("../../config/db.connect.php");
include ("../../config/site.config.php");
include ("../../inc/inc.variable.php");

if (isset($_POST['create'])) :
	$namaSubKategori = filter_var($_POST['namaSubKategory'], FILTER_SANITIZE_STRING);
	$idKat = $_POST['idKategori'];

  if (isset($_FILES['imgKategory'])):
  	$errors= array();
    $file_name = uniqid();
    $file_size =$_FILES['imgKategory']['size'];
    $file_tmp =$_FILES['imgKategory']['tmp_name'];
    $file_type=$_FILES['imgKategory']['type'];
    $file_ext=strtolower(end(explode('.',$_FILES['imgKategory']['name'])));
    $file_name2 = uniqid().".".$file_ext;
    $expensions= array("jpeg","jpg","png");

    if(in_array($file_ext,$expensions)=== false){
         $errors[]="extension not allowed, please choose a JPEG or PNG file.";
    }
  endif; // if there is an icon

  if(empty($errors)==true){
        $query = "INSERT INTO zp_subkategori(subkt_name, subkt_idkategori, subkt_ikon) VALUES ('$namaSubKategori', '$idKat', '$file_name2')";
        $run = pg_query($connect, $query);

        if ($run) :
          move_uploaded_file($file_tmp,"../../upload/icon/subkategory/".$file_name2);
          echo "Success";
        else:
          echo pg_last_error($connect);
        endif;



     }else{
        print_r($errors);
     }

     header("location: ../index.php?page=subKategoriList");

endif; // if there is a submit

if($actionGet == 'delete') :
	$id = $_GET['id'];

	$q_fileName = "SELECT * FROM zp_subkategori WHERE subkt_id = $id";
	$r_fileName = pg_query($q_fileName);
	$data_fileName = pg_fetch_assoc($r_fileName);

	if ($data_fileName['subkt_ikon']) {

    	if (unlink("../../upload/icon/subkategory/".$data_fileName['subkt_ikon'])) {
    		$query = "DELETE FROM zp_subkategori WHERE subkt_id = $id";
			$run = pg_query($query);

			if (pg_affected_rows($run) > 0) {
				echo "Berhasil menghapus data";
			}else{
				echo "Gagal menghapus data";
			}
    		# code...
    	}

    	
	}else{
		$query = "DELETE FROM zp_subkategori WHERE subkt_id = $id";
		$run = pg_query($query);

		if (pg_affected_rows($run) > 0) {
			echo "Berhasil menghapus data";
		}else{
			echo "Gagal menghapus data";
		}
	}

	
endif; // if delete


if (isset($_POST['edit'])) :
  $kategori_nama = $_POST['namaSubKategory'];
  $file_path = "../".$_POST['file_path'];
  $idKat = $_POST['idKategori'];

  if (is_uploaded_file($_FILES['imgKategory']['tmp_name'])) :
    $errors= array();
    $file_name = uniqid();
    $file_size =$_FILES['imgKategory']['size'];
    $file_tmp =$_FILES['imgKategory']['tmp_name'];
    $file_type=$_FILES['imgKategory']['type'];
    $file_ext=strtolower(end(explode('.',$_FILES['imgKategory']['name'])));
    $file_name2 = uniqid().".".$file_ext;
    $expensions= array("jpeg","jpg","png");

    if(in_array($file_ext,$expensions)=== false):
         $errors[]="extension not allowed, please choose a JPEG or PNG file.";
    endif; // if ekstensi file tepat


    if(empty($errors)==true):
        $query = "UPDATE zp_subkategori
                  SET subkt_name = '$kategori_nama', subkt_ikon = '$file_name2', subkt_idkategori = '$idKat'
                  WHERE subkt_id = $idPost";
        $run = pg_query($connect, $query);

        if ($run) :
          unlink($file_path);
          move_uploaded_file($file_tmp,"../../upload/icon/subkategory/".$file_name2);
          echo "Success";
          header("location: ../index.php?page=subKategoriList");
        else:
          echo pg_last_error($connect);
        endif;

     else:
        print_r($errors);
     endif; //end if tak ada error lalu upload

  else:
    $query = "UPDATE zp_subkategori
              SET subkt_name = '$kategori_nama', subkt_idkategori = '$idKat'
              WHERE subkt_id = $idPost";
    $run = pg_query($connect, $query);

    if ($run) :
      echo "Success";
      header("location: ../index.php?page=subKategoriList");
    else:
      echo pg_last_error($connect);
    endif;
    
  endif; //end if ganti ikon

endif; // if edit data ikon
 ?>
