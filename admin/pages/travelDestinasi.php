<?php 
if(!$idGet){ header("location: index.php?page=404"); } 

?>
<div id="page-wrapper">
  <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><?php echo $pageName; ?></h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="index.php?page=dashboard">Dashboard</a></li> 
                <li class="active"><?php echo $pageName; ?></li>
            </ol>
        </div> 
      </div>

      <div class="row">  
        <div class="col-lg-12 col-md-12">
          <div class="white-box">
            <a href="index.php?page=travelList"><button class="btn btn-info waves waves-effect waves-light m-b-10" type="button"> < List Travel </button></a>
            <div class="panel panel-default">
              <div class="panel panel-heading">
                <i class="fa fa-list text-info"></i> &nbsp; Form Destinasi Paket
              </div>
              <div class="panel-wrapper collapse in">


                            <button class="btn btn-default addTravelDestination" type="button"><i class="fa fa-plus text-info"></i> Tambah Destinasi</button>
                      
                <div class="panel-body">
                  <form data-toggle="validator" action="trnsql/sql_travel.php" method="post" enctype="multipart/form-data">
                      <div id="multi-destination">

                      </div>

                      <div class="form-group row">
                          <div class="col-md-12 offset-2">
                            <button class="btn btn-info waves waves-effect" type="submit"  name="travelAddSubmit" id="btnSubmit">Submit</button>
                          </div>
                      </div>



                      </div>
                  </form>

                </div>
                
              </div>

            </div>
          </div>
        </div>
      </div>
  </div>

</div>


<!-- /#page-wrapper -->
<footer class="footer text-center"><?= $footerMessage ?>  </footer>
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="assets/bootstrap/dist/js/tether.min.js"></script>
<script src="assets/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="assets/js/jquery.slimscroll.js"></script>
<!-- price format jquery -->
<script src="assets/js/jquery.priceformat.js"></script>
<!--Wave Effects -->
<script src="assets/js/waves.js"></script>
<!-- validator -->
<script src="assets/js/validator.js"></script>

<!-- Custom Theme JavaScript -->
<script src="assets/js/custom.min.js"></script>

<script src="plugins/bower_components/dropzone-master/dist/dropzone.js"></script>
<script src="plugins/bower_components/html5-editor/wysihtml5-0.3.0.js"></script>
<script src="plugins/bower_components/html5-editor/bootstrap-wysihtml5.js"></script>


<script type="text/javascript">

$(document).ready(function(){

  $('.addTravelDestination').click(function(){

    var append = "<div class='form-group row'> <label for='namaAdmin' class='col-lg-2 col-form-label'>Destinasi 1</label> <div class='col-lg-6'> <select name='destinasi' class='form-control selectDestination'> <option value='0'> -- Pilih Destinasi -- </option> </select> </div><div class='col-md-4'> <button class='btn btn-light removeDestination' type='button'><i class='fa fa-remove text-danger'></i>Hapus</button> </div></div>";

    $('#multi-destination').append(append);

    $('.removeDestination').on('click', function(){
      $(this).parent().parent('div').remove();
    });

    takeDestination();


  });

  function takeDestination(id='')
  {

    $.ajax({
      url: 'trnsql/sql_travel/',
      type: 'get',
      data: {id: id, action: 'getDesstination'},
      success: function(e){

        $('.selectDestination')
        console.log(e);
        alert(e);

      }
    });
    
  }

});

</script>

