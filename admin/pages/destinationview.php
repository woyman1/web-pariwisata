<?php if(!$idGet){header('location: index.php?page=404');} ?> 
<!-- Page Content -->
<style type="text/css">
    .cap{background: rgba(0,0,0,0.5); height: auto; text-align: left; padding-top: 0; padding-bottom: 0; padding-left: 5px; bottom: 40px; border-left: solid cyan 3px;}
    .cap-title{ color: #fff; margin-top: 0; margin-bottom: 0; }
    .cap-desc{margin-top: 0; padding-bottom: 0;}
</style>

        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $pageName; ?></h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        
                        <ol class="breadcrumb">
                            <li><a href="#">Dashboard</a></li>
                            <li class="active"><?php echo $pageName; ?></li>
                        </ol>
                    </div> 
                    <!-- /.col-lg-12 -->

<?php
    $q = "SELECT * FROM destination WHERE dst_id = '$idGet' ";
    $getDestination = mysqli_query($connect, $q);
    $dst = mysqli_fetch_assoc($getDestination);

    $qPhoto = "SELECT * FROM image WHERE dest_id = '$idGet' ";
    $getPhoto = mysqli_query($connect, $qPhoto);
    

        while($photo = mysqli_fetch_array($getPhoto)){
           $image[] = $photo['img_name'];
        }

    $photoCounted = mysqli_num_rows($getPhoto);

   $description =  paragraf($dst['dst_desc']);

   $descCounted = count($description);

 ?>

                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <a href="index.php?page=destinationList"><button class="btn btn-info waves waves-effect waves-light m-b-10" type="button"> < List Destination </button></a>
                            <h3 class="box-title">Lihat Destinasi <?= $dst['dst_name']; ?></h3>
    
                            <div id="carousel-example-captions" data-ride="carousel" class="carousel slide" >

                                <ol class="carousel-indicators" >
                                    <?php for($a = 0; $a < $photoCounted; $a++){ ?>
                                    <li data-target="#carousel-example-captions" data-slide-to="<?= $a; ?>"></li>
                                    <?php } ?>
                                </ol>
                        
                            <div role="listbox" class="carousel-inner" >

                        <?php for($a = 0; $a < $photoCounted; $a++){?>
                               
                                   <div class="carousel-item <?php if($a=='0'){echo 'active';} ?>"> <img src="../assets/img/dst/<?= $image[$a]; ?>" alt="First slide image" width="100%" style="max-height: 620px;">
                                       <div class="carousel-caption cap">
                                        
                                           <h3 class="cap-title" style="height: 50px;"><?= $dst['dst_name']; ?></h3>
                                           <p class="cap-desc"><?= $dst['dst_desc_singkat']; ?></p>
                                        
                                       </div>
                                   </div>
                       <?php   } ?>
                            </div>

                                <a href="#carousel-example-captions" role="button" data-slide="prev" class="left carousel-control"> <span aria-hidden="true" class="fa fa-angle-left"></span> <span class="sr-only">Previous</span> </a>
                                <a href="#carousel-example-captions" role="button" data-slide="next" class="right carousel-control"> <span aria-hidden="true" class="fa fa-angle-right"></span> <span class="sr-only">Next</span> </a>
                            </div>


                            <div class="panel col-9 container">
                                <div class="panel-heading panel-info"><h3><b><?= $dst['dst_name']; ?></b></h3></div>
                                <div class="col-8">
                                    <?php for($a = 0; $a<$descCounted; $a++){ ?>
                                    <p style="text-align: justify;"><?= $description[$a]; ?></p>
                                    <?php
                                        }
                                     ?>
                                </div>
                            </div>

                      <!-- <textarea class="form-control" rows="10"><?php print_r($image); //print_r($getPhoto); echo $photoCounted; ?></textarea>  -->
                        </div>
                    </div>
                </div>
                
            </div>
            <!-- /.container-fluid -->
            
        </div>
        <!-- /#page-wrapper -->
    </div>      
<!-- /#wrapper -->
    <!-- jQuery -->
    <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="assets/bootstrap/dist/js/tether.min.js"></script>
    <script src="assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="assets/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="assets/js/waves.js"></script>
    <!-- jQuery for carousel -->
    <script src="plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
    <!-- <script src="plugins/bower_components/owl.carousel/owl.custom.js"></script> -->
    <!-- Custom Theme JavaScript -->
    <script src="assets/js/custom.min.js"></script>
    <!--Style Switcher -->
    <script src="plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>

    
