<?php
$qgetDestiName = "SELECT web_logo FROM setting where id = 1 ";
    $getDestiName = mysqli_query($connect, $qgetDestiName);
    $logo = mysqli_fetch_assoc($getDestiName);        
 ?> 

<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <h4 class="page-title"><?php echo $pageName; ?></h4>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                        
                        <ol class="breadcrumb">
                            <li><a href="#">Dashboard</a></li>
                            <li class="active"></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title">Logo Utama</h3>
                              <form action="trnsql/sql_setting.php" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="action" value="changeLogo">
                                <div class="form-group row">
                                  <div class="col-md-12 row">
                                    <label style="margin: 10px;">Ganti Logo Utama Disini</label>
                                    <div class="col-md-6">
                                      <input type="file" name="logoWeb" class="form-control">
                                    </div>
                                  </div>
                                 </div>

                                <div class="form-group row">
                                  <div class="col-md-12">
                                    <button class="btn btn-info waves waves-effect pull-right" type="submit"  name="submit" id="btnSubmit">Tambahkan</button>
                                  </div>
                                 </div>

                              </form>

                              <div class="col-md-12">

                                <img src="<?php $logo['web_logo'] ?>">

                              </div>
                                
                              </div>
                        </div>
                    </div>
                
                <!-- <textarea class="form-control" rows="10"> <?php //print_r($qImage) ?></textarea> -->
                </div>
            </div>
            <!-- /.container-fluid -->
            
        </div>

        <!-- /#page-wrapper -->
        <footer class="footer text-center"> <?= $footerMessage ?> </footer>
    </div>      
    <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>

    <script src="assets/bootstrap/dist/js/tether.min.js"></script>
    <script src="assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->

    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
    <script src="plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
    <!--Wave Effects -->
    <script src="assets/js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="assets/js/custom.min.js"></script>
    
    
    <script src="plugins/bower_components/dropzone-master/dist/dropzone.js"></script>
    
<script type="text/javascript">

Dropzone.options.myAwesomeDropzone = false;
Dropzone.autoDiscover = false;

/*jQuery("#actUpload").val()*/
var action = "addNewPhoto";
var idDesti = '<?php echo $idGet ;?>';

var myDropzone = new Dropzone('#dropzone', { 
  url: "trnsql/sql_travel.php",       
  parallelUploads: 10,
  maxFiles: 10,
  maxThumbnailFilesize: 5,
  uploadMultiple: true,               
  autoProcessQueue: false,
  acceptedFiles: ".jpg, .jpeg, .png",
  maxFileSize: 5000,
  addRemoveLinks: true,       
  previewContainer: "#dropzone",
  dictDefaultMessage: 'Seret Gambar atau click untuk menambahkan foto.<br> Max Upload 10 file. ',
  sendingmultiple: function(file, xhr, formData) {
    formData.append("action", action);
    formData.append("id", idDesti);
  }
});

myDropzone.on("queuecomplete", function(file, res){
    // alert(file);
  if(myDropzone.files[0].status == Dropzone.SUCCESS)
  {
    alert("Images Uploaded! ");
    window.location.reload();
    // window.location = 'index.php?page=destinationList';
      
  }else{
    alert("upload file failed");
  }

});


$('#btnSubmit').click(function(e){           

  e.preventDefault();
    e.stopPropagation();

    if (myDropzone.files.length ==  0 ) {
       alert("tidak ada gambar");
    }else if((myDropzone.files.length > 0) && ($('#desc').val() == '') || ($('#name').val() == '')){
        alert("deskripsi atau nama belum diisi");
    } else {
      myDropzone.processQueue();  
    }

});

$(".checkCoverDst").change(function(e){
    var action = "coverTravel";
    var id = $(this).val();
    var ID = <?= $idGet; ?>

  $.ajax({
      url: 'trnsql/sql_travel.php',
      type: 'post',
      data: {action: action, id: id, ID: ID },
      success: function(e){

        alert(e);

      },
  });

});


</script>

