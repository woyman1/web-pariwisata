<!-- <link href="plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" /> -->
<div id="page-wrapper">
  <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><?php echo $pageName; ?></h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="index.php?page=dashboard">Dashboard</a></li> 
                <li class="active"><?php echo $pageName; ?></li>
            </ol>
        </div> 
      </div>

      <div class="row">  
        <div class="col-lg-12 col-md-12">
          <div class="white-box">
            <div class="panel panel-info">
              <div class="panel panel-heading text-light">
                Informasi Situs
              </div>
              <div class="panel-wrapper collapse in">
                <div class="panel-body">
                  <form data-toggle="validator" action="trnsql/sql_setting.php" method="post" enctype="multipart/form-data">

                      <div class="form-group row">
                        <label for="namaAdmin" class="col-lg-2 col-form-label">Site Name</label>
                        <div class="col-lg-10">
                          <input type="text" class="form-control" name="situs_name" placeholder="Nama Situs" value="<?= $_SESSION['web_name'];?>" required>
                        </div>
                      </div>

                       <div class="form-group row">
                        <label for="namaAdmin" class="col-lg-2 col-form-label">Deskripsi Tentang Situs</label>
                        <div class="col-lg-10">
                         <textarea class="form-control" id="desc" name="situs_desc"  rows="6"><?= $_SESSION['web_desc'];?></textarea>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="namaAdmin" class="col-lg-2 col-form-label">Nama Perusahaan</label>
                        <div class="col-lg-10">
                          <input type="text" class="form-control" name="corp_name" placeholder="Nama perusahaan" value="<?= $_SESSION['web_corp'];?>" required>
                        </div>
                      </div>

                       <div class="form-group row">
                        <label for="picAdmin" class="col-lg-2 col-form-label">Alamat Perusahaan</label>
                        <div class="col-lg-10">
                          <input type="text" class="form-control" name="corp_address" value="<?= $_SESSION['web_corp_address'];?>">
                        </div>
                      </div>

                       <div class="form-group row">
                        <label for="picAdmin" class="col-lg-2 col-form-label">Email </label>
                        <div class="col-lg-10">
                          <input type="text" class="form-control" name="corp_email" placeholder="email@example.com" value="<?= $_SESSION['web_email'];?>">
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="picAdmin" class="col-lg-2 col-form-label">No. Kontak</label>
                        <div class="col-lg-10">
                          <input type="text" class="form-control" name="no_telp" placeholder="5232342 / 08123123432" value="<?= $_SESSION['web_phone'];?>">
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="picAdmin" class="col-lg-2 col-form-label">No. WA</label>
                        <div class="col-lg-10">
                          <input type="text" class="form-control" name="no_WA" placeholder="0812312334312" value="<?= $_SESSION['web_WA'];?>">
                        </div>
                      </div>

                      <div class="form-group row">
                          <div class="col-md-12 offset-2">
                            <button class="btn btn-info waves waves-effect" type="submit"  name="SubmitSiteSetting" id="SubmitSiteSetting">Simpan</button>
                          </div>
                      </div>



                      </div>
                  </form>

                </div>
                
              </div>

            </div>
          </div>
        </div>
      </div>
  </div>

</div>


<!-- /#page-wrapper -->
<footer class="footer text-center"><?= $footerMessage ?>  </footer>
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="assets/bootstrap/dist/js/tether.min.js"></script>
<script src="assets/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="assets/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="assets/js/waves.js"></script>
<!-- validator -->
<script src="assets/js/validator.js"></script>

<!-- Custom Theme JavaScript -->
<script src="assets/js/custom.min.js"></script>

<script src="plugins/bower_components/html5-editor/wysihtml5-0.3.0.js"></script>
<script src="plugins/bower_components/html5-editor/bootstrap-wysihtml5.js"></script>

<script type="text/javascript">
  
  $(document).ready(function() {
        $('#desc').wysihtml5();
});

</script>