<?php if(!$idGet){ header("location: index.php?page=404"); } 
$qgetDestiName = "SELECT dst_name FROM destination where dst_id = '$idGet' ";
    $getDestiName = mysqli_query($connect, $qgetDestiName);
    $destiName = mysqli_fetch_assoc($getDestiName);        
 ?> 

<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <h4 class="page-title"><?php echo $pageName; ?></h4>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                        
                        <ol class="breadcrumb">
                            <li><a href="#">Dashboard</a></li>
                            <li class="active"></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title">Upload foto untuk <?php echo $destiName['dst_name']; ?></h3>
                            
                              <div class="form-group row">
                                <div class="col-lg-12">
                                  <div class="dropzone-previews dropzone" id="dropzone">
                                    <div class="fallback"></div>
                                  </div> 
                                </div>
                              </div>
                             <div class="form-group row">
                              <div class="col-md-12">
                                <button class="btn btn-info waves waves-effect pull-right" type="submit"  name="submit" id="btnSubmit">Tambahkan</button>
                              </div>
                             </div>
                        </div>
                    </div>
                
    
                    <div class="col-md-12 row">
        
                        <?php 
                            $q = "SELECT * FROM image WHERE dest_id = '$idGet' ";
                            $qImage = mysqli_query($connect, $q);
                            while($image =mysqli_fetch_assoc($qImage)){
                        ?>
                            <div class="col-md-3">
                                <div class="col-md-12 white-box">
                                   <a href="../assets/img/dst/<?php echo $image['img_name'];?>" title=" " class="image-popup-no-margins"> 
                                    <img class="img-responsive" src="../assets/img/dst/<?php echo $image['img_name'];?>"></a>
                                    <a href="trnsql/sql_destination.php?action=deleteImg&id=<?= $image['img_id']; ?>&ID=<?= $idGet; ?>"><button class="btn btn-danger" style="margin-top:5px;">Delete</button></a>
                                    <div class="pull-right col-md-8">
                                      <label class="control-label col-md-12"><input type="checkbox" class="checkSlideshow" value="<?= $image['img_id']; ?>" <?php if($image['slideshow'] == '1') echo "checked"  ?>> Slideshow</label>
                                      <label class="control-label col-md-12"><input type="radio" class="checkCoverDst" name="cover" value="<?= $image['img_id']; ?>" <?php if($image['img_cover']==1) echo "checked"; ?> >Cover</label>
                                  </div>
                                </div>
                            </div>     

                        <?php } ?>                   
                    </div>
                <!-- <textarea class="form-control" rows="10"> <?php //print_r($qImage) ?></textarea> -->
                </div>
            </div>
            <!-- /.container-fluid -->
            
        </div>

        <!-- /#page-wrapper -->
        <footer class="footer text-center"> <?= $footerMessage ?> </footer>
    </div>      
    <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>

    <script src="assets/bootstrap/dist/js/tether.min.js"></script>
    <script src="assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->

    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
    <script src="plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
    <!--Wave Effects -->
    <script src="assets/js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="assets/js/custom.min.js"></script>
    
    
    <script src="plugins/bower_components/dropzone-master/dist/dropzone.js"></script>
    
<script type="text/javascript">

Dropzone.options.myAwesomeDropzone = false;
Dropzone.autoDiscover = false;

/*jQuery("#actUpload").val()*/
var action = "addNewPhoto";
var idDesti = '<?php echo $idGet ;?>';

var myDropzone = new Dropzone('#dropzone', { 
  url: "trnsql/sql_destination.php",       
  parallelUploads: 10,
  maxFiles: 10,
  maxThumbnailFilesize: 5,
  uploadMultiple: true,               
  autoProcessQueue: false,
  acceptedFiles: ".jpg, .jpeg, .png",
  maxFileSize: 5000,
  addRemoveLinks: true,       
  previewContainer: "#dropzone",
  dictDefaultMessage: 'Seret Gambar atau click untuk menambahkan foto.<br> Max Upload 10 file. ',
  sendingmultiple: function(file, xhr, formData) {
    formData.append("action", action);
    formData.append("id", idDesti);
  }
});

myDropzone.on("queuecomplete", function(file, res){
    // alert(file);
  if(myDropzone.files[0].status == Dropzone.SUCCESS)
  {
    alert("Images Uploaded! ");
    window.location.reload();
    // window.location = 'index.php?page=destinationList';
      
  }else{
    alert("upload file failed");
  }

});


$('#btnSubmit').click(function(e){           

  e.preventDefault();
    e.stopPropagation();

    if (myDropzone.files.length ==  0 ) {
       alert("tidak ada gambar");
    }else if((myDropzone.files.length > 0) && ($('#desc').val() == '') || ($('#name').val() == '')){
        alert("deskripsi atau nama belum diisi");
    } else {
      myDropzone.processQueue();  
    }

});

$(".checkSlideshow").change(function(e){
    var action = "slideshow";
    var id = $(this).val();

  if ($(this).is(":checked")) {
    var check = 1;
  }
  else{
    var check = 0;
  }

  $.ajax({
      url: 'trnsql/sql_destination.php',
      type: 'post',
      data: {action: action, id: id, check: check },
      success: function(e){

        alert(e);

      },
  });

});

$(".checkCoverDst").change(function(e){
    var action = "coverDst";
    var id = $(this).val();
    var ID = <?= $idGet; ?>

  $.ajax({
      url: 'trnsql/sql_destination.php',
      type: 'post',
      data: {action: action, id: id, ID: ID },
      success: function(e){

        alert(e);

      },
  });

});


</script>

