<?php if(!$idGet){header('location: index.php?page=404');} ?> 
<div id="page-wrapper">
  <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><?php echo $pageName; ?></h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb"> 
                <li><a href="index.php?page=dashboard">Dashboard</a></li>
                <li class="active"><?php echo $pageName; ?></li>
            </ol>
        </div>
      </div>

<?php $q = "SELECT * FROM destination WHERE dst_id = '$idGet' ";
      $getDestination = mysqli_query($connect, $q);
      $dst = mysqli_fetch_assoc($getDestination); 
      $desc = paragraf($dst['dst_desc']);
      $loop = count($desc); 
 ?>

      <div class="row">
        <div class="col-lg-12 col-md-12"> 
          <div class="white-box">
            <a href="index.php?page=destinationList"><button class="btn btn-info waves waves-effect waves-light m-b-10" type="button"> < List Destination </button></a>
            <div class="panel panel-default">
              <div class="panel panel-heading">
                Form Edit Destinasi
              </div>
              <div class="panel-wrapper collapse in">
                <div class="panel-body">
                  <form data-toggle="validator" method="post" enctype="multipart/form-data" action="trnsql/sql_destination.php">
                      <input type="hidden" name="action" value="editDestination">
                      <input type="hidden" name="id" value="<?php echo $idGet; ?>">
                      <div class="form-group row">
                        <label for="namaAdmin" class="col-lg-2 col-form-label">Nama Destinasi</label>
                        <div class="col-lg-10">
                          <input type="text" class="form-control" name="name_dst" id="name" placeholder="Borobudur misalkan" value="<?= $dst['dst_name']; ?>" required>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="namaAdmin" class="col-lg-2 col-form-label">Lokasi Destinasi</label>
                        <div class="col-lg-10">
                          <input type="text" class="form-control" name="location_dst" id="name" placeholder="Borobudur misalkan" value="<?= $dst['dst_location']; ?>" required>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="namaAdmin" class="col-lg-2 col-form-label">Deskripsi Singkat</label>
                        <div class="col-lg-10">
                          <input type="text" class="form-control" name="desc_dst_singkat" id="desc_singkat" placeholder="Deskripsi Singkat" value="<?= $dst['dst_desc_singkat']; ?>" required>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="usernameAdmin" class="col-lg-2 col-form-label">Deskripsi/Penjelesan</label>
                        <div class="col-lg-10">
                            <textarea class="form-control" name="desc_dst" id="desc" placeholder="Deskripsi disini... " rows="15">
                              
                              <?php 
                              for($a=0; $a < $loop; $a++){ 
                                echo $desc[$a]."<br>";
                              } 

                              ?>
                                
                              </textarea>
                          <span class="help-block with-errors"></span>
                        </div>
                      </div>

                      <div class="form-group row">
                          <div class="col-md-12 offset-2">
                            <button class="btn btn-info waves waves-effect" type="submit"  name="submit" id="btnSubmit">Simpan</button>
                          </div>
                      </div>



                      </div>
                  </form>

                </div>
                <div class="panel-footer">

                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
  </div>

</div>


<!-- /#page-wrapper -->
<footer class="footer text-center"> <?= $footerMessage ?> </footer>
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="assets/bootstrap/dist/js/tether.min.js"></script>
<script src="assets/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="assets/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="assets/js/waves.js"></script>
<!-- validator -->
<script src="assets/js/validator.js"></script>

<!-- Custom Theme JavaScript -->
<script src="assets/js/custom.min.js"></script>
<script src="plugins/bower_components/html5-editor/wysihtml5-0.3.0.js"></script>
<script src="plugins/bower_components/html5-editor/bootstrap-wysihtml5.js"></script>


<script type="text/javascript">

$(document).ready(function() {
        $('#desc').wysihtml5();
});


</script>

