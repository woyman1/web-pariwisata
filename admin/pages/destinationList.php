<div id="page-wrapper">
  <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><?php echo $pageName; ?></h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="index.php?page=dashboard">Dashboard</a></li>
                <li class="active"><?php echo $pageName; ?></li>
            </ol>
        </div>
      </div> 

      <div class="row">
        <div class="col-lg-12 col-md-12"> 
          <div class="white-box">
            
            <div class="panel panel-default">
              <div class="panel panel-heading">
                <i style="color:blue" class=" ti-map-alt fa-2x"></i>&nbsp; &nbsp; <?php echo $pageName; ?>
              </div>
              <div class="panel-wrapper collapse in">
                <div class="panel-body">
                  <dic class="table-responsive">
                   
                    <table id="myTable" class="table table-striped">
                      <thead>
                        <tr>
                          <th>Destination Name</th>
                          <th>Location</th>
                          <th>Date Post</th>
                          <th>Action</th>
                          
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                            $query = mysqli_query($connect, "SELECT * FROM destination ORDER BY dst_date_post DESC");

                            if($query)
                            {
                              while($data = mysqli_fetch_array($query))
                              { ?> 
                              <tr>
                                <td><?= $data['dst_name']; ?></td>
                                <td><?= $data['dst_location']; ?></td>
                                <td><?= $data['dst_date_post']; ?></td>
                                <td> 
                                     <a href="index.php?page=destinationView&id=<?= $data['dst_id']; ?>" data-toggle="tooltip" data-original-title="Lihat"><i class="fa fa-search text-muted m-r-10"></i></a>
                                     <a href="index.php?page=destinationEdit&id=<?= $data['dst_id']; ?>" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-edit text-success m-r-10"></i></a>
                                      <a href="index.php?page=destinationUploadPhoto&id=<?= $data['dst_id']; ?>" data-toggle="tooltip" data-original-title="Images"><i class="fa fa-photo text-info m-r-10"></i></a>
                                     <a href="trnsql/sql_destination.php?action=deleteDestination&id=<?= $data['dst_id']; ?>" data-toggle="tooltip" data-original-title="Hapus "><i class="fa fa-close text-danger m-r-10"></i></a>
                                     
                              </tr>
                        <?php }
                            }
                            else{
                              echo "Gagal masuk";
                            }

                         ?>
                      </tbody>

                    </table>
                  </dic>

                </div>
                <div class="panel-footer">

                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
  </div>

</div>

<!-- /#page-wrapper -->

</div>
    <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="assets/bootstrap/dist/js/tether.min.js"></script>
    <script src="assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    
    <script src="plugins/bower_components/datatables/jquery.dataTables.min.js"></script>

    <!--slimscroll JavaScript -->

    <script src="assets/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="assets/js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="assets/js/custom.min.js"></script>
    

<script>
    $(document).ready(function() {
        $('#myTable').DataTable({
            "order": [
                    [2, 'desc']
            ],
            dom: 'Bfrtip'
          });
    });

</script>