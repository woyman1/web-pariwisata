<!-- <link href="plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" /> -->
<div id="page-wrapper">
  <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><?php echo $pageName; ?></h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="index.php?page=dashboard">Dashboard</a></li> 
                <li class="active"><?php echo $pageName; ?></li>
            </ol>
        </div>
      </div>

      <div class="row">  
        <div class="col-lg-12 col-md-12">
          <div class="white-box">
            <a href="index.php?page=destinationList"><button class="btn btn-info waves waves-effect waves-light m-b-10" type="button"> < List Destination </button></a>
            <div class="panel panel-default">
              <div class="panel panel-heading">
                <i class="fa fa-plus-square text-info"></i> &nbsp; Form Tambah Destinasi Baru
              </div>
              <div class="panel-wrapper collapse in">
                <div class="panel-body">
                  <form data-toggle="validator" method="post" enctype="multipart/form-data">

                      <div class="form-group row">
                        <label for="namaAdmin" class="col-lg-2 col-form-label">Nama Destinasi</label>
                        <div class="col-lg-10">
                          <input type="text" class="form-control" name="name_dst" id="name" placeholder="Borobudur misalkan" required>
                        </div>
                      </div>

                       <div class="form-group row">
                        <label for="namaAdmin" class="col-lg-2 col-form-label">Lokasi Destinasi</label>
                        <div class="col-lg-10">
                          <input type="text" class="form-control" name="location_dst" id="lokasi" placeholder="Magelang, Central Java" required>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="namaAdmin" class="col-lg-2 col-form-label">Deskripsi Singkat</label>
                        <div class="col-lg-10">
                          <input type="text" class="form-control" name="desc_dst_singkat" id="desc_singkat" placeholder="Deskripsi Singkat" required>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="usernameAdmin" class="col-lg-2 col-form-label">Deskripsi/Penjelesan</label>
                        <div class="col-lg-10">
                            <textarea class="form-control" name="desc_dst" id="desc" placeholder="Deskripsi disini... " rows="15"></textarea>
                          <span class="help-block with-errors"></span>
                        </div>
                      </div>

                       <div class="form-group row">
                        <label for="picAdmin" class="col-lg-2 col-form-label">Gambar/Foto</label>
                        <div class="col-lg-10">
                          <div class="dropzone-previews dropzone" id="dropzone"><div class="fallback"></div>     </div>
                        </div>
                      </div>

                      <div class="form-group row">
                          <div class="col-md-12 offset-2">
                            <button class="btn btn-info waves waves-effect" type="submit"  name="submit" id="btnSubmit">Tambahkan</button>
                          </div>
                      </div>



                      </div>
                  </form>

                </div>
                
              </div>

            </div>
          </div>
        </div>
      </div>
  </div>

</div>


<!-- /#page-wrapper -->
<footer class="footer text-center"><?= $footerMessage ?>  </footer>
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="assets/bootstrap/dist/js/tether.min.js"></script>
<script src="assets/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="assets/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="assets/js/waves.js"></script>
<!-- validator -->
<script src="assets/js/validator.js"></script>

<!-- Custom Theme JavaScript -->
<script src="assets/js/custom.min.js"></script>

<script src="plugins/bower_components/dropzone-master/dist/dropzone.js"></script>
<script src="plugins/bower_components/html5-editor/wysihtml5-0.3.0.js"></script>
<script src="plugins/bower_components/html5-editor/bootstrap-wysihtml5.js"></script>


<script type="text/javascript">

$(document).ready(function() {
        $('#desc').wysihtml5();
});


Dropzone.options.myAwesomeDropzone = false;
Dropzone.autoDiscover = false;

/*jQuery("#actUpload").val()*/
var action = "newDesti";


var myDropzone = new Dropzone('#dropzone', { 
  url: "trnsql/sql_destination.php",       
  parallelUploads: 10,
  maxFiles: 10,
  maxThumbnailFilesize: 5,
  uploadMultiple: true,               
  autoProcessQueue: false,
  acceptedFiles: ".jpg, .jpeg, .png",
  maxFileSize: 5000,
  addRemoveLinks: true,       
  previewContainer: "#dropzone",
  dictDefaultMessage: 'Seret Gambar atau click untuk memilih. Max Upload 10 file <br> Lebih ',
  sendingmultiple: function(file, xhr, formData) {
    formData.append("action", action);
    formData.append("desc_dst", $('#desc').val() );
    formData.append("location_dst", $('#lokasi').val() );
    formData.append("name_dst", $('#name').val() );
    formData.append("desc_dst_singkat", $('#desc_singkat').val() );
  }
});

myDropzone.on("queuecomplete", function(file, res){

  if(myDropzone.files[0].status == Dropzone.SUCCESS)
  {
    alert("Images Uploaded! ");
    window.location = 'index.php?page=destinationList';
      
  }else{
    alert("upload file failed");
  }

});



$('#btnSubmit').click(function(e){           

  e.preventDefault();
    e.stopPropagation();

    if (myDropzone.files.length ==  0 ) {
       alert("tidak ada gambar");
    }else if((myDropzone.files.length > 0) && ($('#desc').val() == '') || ($('#name').val() == '')){
        alert("deskripsi atau nama belum diisi");
    } else {
      myDropzone.processQueue();  
    }

});


</script>

