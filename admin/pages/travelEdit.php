<?php if(!$idGet){ header("location: index.php?page=404"); } 
$qgetDestiName = "SELECT * FROM paket_tour where id_paket = '$idGet' ";
    $getDestiName = mysqli_query($connect, $qgetDestiName);
    $paket = mysqli_fetch_assoc($getDestiName);        
 ?> 

<div id="page-wrapper">
  <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><?php echo $pageName; ?></h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="index.php?page=dashboard">Dashboard</a></li> 
                <li class="active"><?php echo $pageName; ?></li>
            </ol>
        </div> 
      </div>

      <div class="row">  
        <div class="col-lg-12 col-md-12">
          <div class="white-box">
            <a href="index.php?page=travelList"><button class="btn btn-info waves waves-effect waves-light m-b-10" type="button"> < List Travel </button></a>
            <div class="panel panel-default">
              <div class="panel panel-heading">
                <i class="fa fa-plus-square text-info"></i> &nbsp; Form Edit Paket Tour</div>
              <div class="panel-wrapper collapse in">
                <div class="panel-body">
                  <form data-toggle="validator" action="trnsql/sql_travel.php" method="post" enctype="multipart/form-data">
                      <input type="hidden" name="id" value="<?= $idGet ?>">
                      <div class="form-group row">
                        <label for="namaAdmin" class="col-lg-2 col-form-label">Nama Paket</label>
                        <div class="col-lg-10">
                          <input type="text" class="form-control" name="name_paket" id="name" value="<?= $paket['paket_name'] ?>" required>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="usernameAdmin" class="col-lg-2 col-form-label">Highlight</label>
                        <div class="col-lg-10">
                            <textarea class="form-control desc" name="highlight" id="highlight" placeholder="Deskripsi disini... " rows="15"><?= $paket['highlight']; ?></textarea>
                          <span class="help-block with-errors"></span>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="usernameAdmin" class="col-lg-2 col-form-label">Itenary</label>
                        <div class="col-lg-10">
                            <textarea class="form-control" name="itenary" id="itenary" rows="15"><?= $paket['itenaery']; ?></textarea>
                          <span class="help-block with-errors"></span>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="usernameAdmin" class="col-lg-2 col-form-label">Facility</label>
                        <div class="col-lg-10">
                            <textarea class="form-control desc" name="facility" id="facility" placeholder="Deskripsi disini... " rows="15"><?= $paket['facilities']; ?></textarea>
                          <span class="help-block with-errors"></span>
                        </div>
                      </div>

                       <div class="form-group row">
                        <label for="usernameAdmin"  class="col-lg-2 col-form-label">Harga IDR</label>
                        <div class="col-lg-10">
                            <input id="idr" class="form-control" type="text" value="<?= $paket['idr_price'] ?>" name="price_idr">
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="usernameAdmin" class="col-lg-2 col-form-label">Harga Dollar USD</label>
                        <div class="col-lg-10">
                            <input id="dollar" class="form-control" type="text" value="<?= $paket['usd_price'] ?>" name="price_dollar">
                        </div>
                      </div>

                       <!-- <div class="form-group row">
                        <label for="picAdmin" class="col-lg-2 col-form-label">Gambar/Foto</label>
                        <div class="col-lg-10">
                          <div class="dropzone-previews dropzone" id="dropzone"><div class="fallback"></div>     </div>
                        </div>
                      </div> -->

                      <div class="form-group row">
                          <div class="col-md-12 offset-2">
                            <button class="btn btn-info waves waves-effect" type="submit"  name="travelEditSubmit" id="btnSubmit">Tambahkan</button>
                          </div>
                      </div>



                      </div>
                  </form>

                </div>
                
              </div>

            </div>
          </div>
        </div>
      </div>
  </div>

</div>


<!-- /#page-wrapper -->
<footer class="footer text-center"><?= $footerMessage ?>  </footer>
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="assets/bootstrap/dist/js/tether.min.js"></script>
<script src="assets/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="assets/js/jquery.slimscroll.js"></script>
<!-- price format jquery -->
<script src="assets/js/jquery.priceformat.js"></script>
<!--Wave Effects -->
<script src="assets/js/waves.js"></script>
<!-- validator -->
<script src="assets/js/validator.js"></script>

<!-- Custom Theme JavaScript -->
<script src="assets/js/custom.min.js"></script>

<script src="plugins/bower_components/dropzone-master/dist/dropzone.js"></script>
<script src="plugins/bower_components/html5-editor/wysihtml5-0.3.0.js"></script>
<script src="plugins/bower_components/html5-editor/bootstrap-wysihtml5.js"></script>


<script type="text/javascript">

$(document).ready(function() {
        $('#highlight').wysihtml5();
        $('#itenary').wysihtml5();
        $('#facility').wysihtml5();

        $('#idr').priceFormat({
          prefix: 'Rp. ',
          clearPrefix: true,  
          thousandsSeparator: '.',
          centsLimit: 0,
          clearOnEmpty: true,
        });

        $('#dollar').priceFormat({
          prefix: '$ ',
          clearPrefix: true,  
          thousandsSeparator: '.',
          centsLimit: 0,
          clearOnEmpty: true,
        });
});


Dropzone.options.myAwesomeDropzone = false;
Dropzone.autoDiscover = false;

/*jQuery("#actUpload").val()*/
var action = "newDesti";


var myDropzone = new Dropzone('#dropzone', { 
  url: "trnsql/sql_destination.php",       
  parallelUploads: 10,
  maxFiles: 10,
  maxThumbnailFilesize: 5,
  uploadMultiple: true,               
  autoProcessQueue: false,
  acceptedFiles: ".jpg, .jpeg, .png",
  maxFileSize: 5000,
  addRemoveLinks: true,       
  previewContainer: "#dropzone",
  dictDefaultMessage: 'Seret Gambar atau click untuk memilih. Max Upload 10 file <br> Lebih ',
  sendingmultiple: function(file, xhr, formData) {
    formData.append("action", action);
    formData.append("desc_dst", $('#desc').val() );
    formData.append("location_dst", $('#lokasi').val() );
    formData.append("name_dst", $('#name').val() );
    formData.append("desc_dst_singkat", $('#desc_singkat').val() );
  }
});

myDropzone.on("queuecomplete", function(file, res){

  if(myDropzone.files[0].status == Dropzone.SUCCESS)
  {
    alert("Images Uploaded! ");
    window.location = 'index.php?page=destinationList';
      
  }else{
    alert("upload file failed");
  }

});



$('#btnSubmit').click(function(e){           

  e.preventDefault();
    e.stopPropagation();

    if (myDropzone.files.length ==  0 ) {
       alert("tidak ada gambar");
    }else if((myDropzone.files.length > 0) && ($('#desc').val() == '') || ($('#name').val() == '')){
        alert("deskripsi atau nama belum diisi");
    } else {
      myDropzone.processQueue();  
    }

});


</script>

