-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 19, 2018 at 07:20 AM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hawahuholidayjogja`
--

-- --------------------------------------------------------

--
-- Table structure for table `destination`
--

DROP TABLE IF EXISTS `destination`;
CREATE TABLE IF NOT EXISTS `destination` (
  `dst_id` int(100) NOT NULL AUTO_INCREMENT,
  `dst_name` varchar(255) NOT NULL,
  `dst_location` varchar(255) DEFAULT NULL,
  `dst_desc_singkat` varchar(244) DEFAULT NULL,
  `dst_desc` text NOT NULL,
  `dst_date_post` datetime NOT NULL,
  `dst_pack_travel_id` int(100) DEFAULT NULL,
  `dst_image_cover` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`dst_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `destination`
--

INSERT INTO `destination` (`dst_id`, `dst_name`, `dst_location`, `dst_desc_singkat`, `dst_desc`, `dst_date_post`, `dst_pack_travel_id`, `dst_image_cover`) VALUES
(9, 'Borobudur', 'Mungkid, Magelang, Central Java', 'Borobudur is best seen at dawn, when the air is fresh and full of birdsong. ', '<div>Borobudur Sunrise</div><div>Borobudur is best seen at dawn, when the air is fresh and full of birdsong. As the mist begins to lift, the sun scales the surrounding volcanoes and terraced fields, and highlights the stone reliefs and the many faces of Buddha. Glimpsing Borobudur for the first time is often a deeply felt emotional experience.</div><div><br></div><div>This tour offers a different experience where participants were given a special pass to enter the monument early in the morning at approximately 04.30 am before public visitors and visitor have to walk from the hotel to Borobudur with a pre-dawn stroll through the dewy grass of a lush tropical garden. A panoramic scenary and a very beautiful morning at Borobudur especially those moments when the sun would appear from the horizon. A truly enlightening experience.</div><br><br>', '2018-07-20 14:49:40', NULL, '5b51f6849660d.jpg'),
(10, 'Prambanan', 'Klaten, Central Java', 'Prambanan Temple Compounds consist of Prambanan Temple (also called Loro Jonggrang), Sewu Temple, Bubrah Temple and Lumbung Temple. Prambanan Temple itself is a complex consisting of 240 temples. ', 'Prambanan Temple Compounds consist of Prambanan Temple (also called Loro Jonggrang), Sewu Temple, Bubrah Temple and Lumbung Temple. Prambanan Temple itself is a complex consisting of 240 temples. All the mentioned temples form the Prambanan Archaeological Park and were built during the heyday of Sailendraâ€™s powerful dynasty in Java in the 8th century AD. These compounds are located on the border between the two provinces of Yogyakarta and Central Java on Java Island.While Loro Jonggrang, dating from the 9th century, is a brilliant example of Hindu religious bas-reliefs, Sewu, with its four pairs of Dwarapala giant statues, is Indonesiaâ€™s largest Buddhist complex including the temples of Lumbung, Bubrah and Asu (Gana temple). The Hindu temples are decorated with reliefs illustrating the Indonesian version of the Ramayana epic which are masterpieces of stone carvings. These are surrounded by hundreds of shrines that have been arranged in three parts showing high levels of stone building technology and architecture from the 8th century AD in Java. With over 500 temples, Prambanan Temple Compounds represents not only an architectural and cultural treasure, but also a standing proof of past religious peaceful cohabitation.<br>', '2018-07-22 11:32:08', NULL, '5b546b38a97af.jpg'),
(13, 'Merapi', 'Yogyakarta', 'Mount Merapi, Gunung Merapi (literally Fire Mountain in Indonesian and Javanese), is an active stratovolcano located on the border between Central Java and Yogyakarta provinces, Indonesia.', '<span><b>Mount Merapi</b>,&nbsp;<i>Gunung Merapi</i>&nbsp;(literally&nbsp;<b>Fire Mountain</b>&nbsp;in&nbsp;<a target=\"_blank\" rel=\"nofollow\" href=\"https://en.wikipedia.org/wiki/Indonesian_language\">Indonesian</a>&nbsp;and&nbsp;<a target=\"_blank\" rel=\"nofollow\" href=\"https://en.wikipedia.org/wiki/Javanese_language\">Javanese</a>), is an&nbsp;<a target=\"_blank\" rel=\"nofollow\" href=\"https://en.wikipedia.org/wiki/Active_volcano\">active</a>&nbsp;<a target=\"_blank\" rel=\"nofollow\" href=\"https://en.wikipedia.org/wiki/Stratovolcano\">stratovolcano</a>&nbsp;located on the border between&nbsp;<a target=\"_blank\" rel=\"nofollow\" href=\"https://en.wikipedia.org/wiki/Central_Java\">Central Java</a>&nbsp;and&nbsp;<a target=\"_blank\" rel=\"nofollow\" href=\"https://en.wikipedia.org/wiki/Yogyakarta\" title=\"Link: https://en.wikipedia.org/wiki/Yogyakarta\">Yogyakarta</a>&nbsp;provinces,&nbsp;<a target=\"_blank\" rel=\"nofollow\" href=\"https://en.wikipedia.org/wiki/Indonesia\" title=\"Link: https://en.wikipedia.org/wiki/Indonesia\">Indonesia</a>. It is the most active volcano in Indonesia and has erupted regularly since 1548. It is located approximately 28 kilometres (17&nbsp;mi) north of&nbsp;<a target=\"_blank\" rel=\"nofollow\" href=\"https://en.wikipedia.org/wiki/Yogyakarta_(city)\">Yogyakarta</a>&nbsp;city which has a population of 2.4 million, and thousands of people live on the flanks of the volcano, with villages as high as 1,700 metres (5,600&nbsp;ft) above&nbsp;<a target=\"_blank\" rel=\"nofollow\" href=\"https://en.wikipedia.org/wiki/Sea_level\">sea level</a>.</span><span>Smoke can often be seen emerging from the mountaintop, and several eruptions have caused fatalities. Pyroclastic flow from a large explosion killed 27 people on 22 November 1994, mostly in the town of Muntilan, west of the volcano.<a target=\"_blank\" rel=\"nofollow\" href=\"https://en.wikipedia.org/wiki/Mount_Merapi#cite_note-2\">[2]</a>&nbsp;Another large eruption occurred in 2006, shortly before the&nbsp;<a target=\"_blank\" rel=\"nofollow\" href=\"https://en.wikipedia.org/wiki/2006_Yogyakarta_earthquake\" title=\"Link: https://en.wikipedia.org/wiki/2006_Yogyakarta_earthquake\">Yogyakarta earthquake</a>. In light of the hazards that Merapi poses to populated areas, it has been designated as one of the&nbsp;<a target=\"_blank\" rel=\"nofollow\" href=\"https://en.wikipedia.org/wiki/Decade_Volcanoes\" title=\"Link: https://en.wikipedia.org/wiki/Decade_Volcanoes\">Decade Volcanoes</a>.</span><span>On 25 October 2010 the Indonesian government raised the alert for Mount Merapi to its highest level and warned villagers in threatened areas to move to safer ground. People living within the range of a 20&nbsp;km (12&nbsp;mi) zone were told to evacuate. Officials said<span>[<i><a target=\"_blank\" rel=\"nofollow\" href=\"https://en.wikipedia.org/wiki/Wikipedia:Manual_of_Style/Words_to_watch#Unsupported_attributions\" title=\"Link: https://en.wikipedia.org/wiki/Wikipedia:Manual_of_Style/Words_to_watch#Unsupported_attributions\">who?</a></i>]</span>&nbsp;about 500 volcanic earthquakes had been recorded on the mountain over the weekend of 23â€“24 October<span>[<i><a target=\"_blank\" rel=\"nofollow\" href=\"https://en.wikipedia.org/wiki/Wikipedia:Vagueness\">vague</a></i>]</span>, and that the magma had risen to about 1 kilometre (3,300&nbsp;ft) below the surface due to the seismic activity.<a target=\"_blank\" rel=\"nofollow\" href=\"https://en.wikipedia.org/wiki/Mount_Merapi#cite_note-BBC2010-10-25-3\">[3]</a>&nbsp;<a target=\"_blank\" rel=\"nofollow\" href=\"https://en.wikipedia.org/wiki/2010_eruptions_of_Mount_Merapi\" title=\"Link: https://en.wikipedia.org/wiki/2010_eruptions_of_Mount_Merapi\">On the afternoon of 25 October 2010 Mount Merapi erupted</a>&nbsp;lava from its southern and southeastern slopes.<a target=\"_blank\" rel=\"nofollow\" href=\"https://en.wikipedia.org/wiki/Mount_Merapi#cite_note-JakartaPost2010-10-25-4\">[4]</a></span><span>The mountain was still erupting on 30 November 2010, but due to lowered eruptive activity on 3 December 2010 the official alert status was reduced to level 3.<a target=\"_blank\" rel=\"nofollow\" href=\"https://en.wikipedia.org/wiki/Mount_Merapi#cite_note-bnpb-5\">[5]</a>&nbsp;The volcano is now 2930 metres high,<a target=\"_blank\" rel=\"nofollow\" href=\"https://en.wikipedia.org/wiki/Mount_Merapi#cite_note-Gunung_Bagging-1\">[1]</a>&nbsp;38 metres lower than before the 2010 eruptions.</span><span>After a large eruption in 2010 the characteristic of Mount Merapi was changed. On 18 November 2013 Mount Merapi burst smoke up to 2,000 meters high, one of its first major&nbsp;<a target=\"_blank\" rel=\"nofollow\" href=\"https://en.wikipedia.org/wiki/Phreatic_eruption\">phreatic eruptions</a>&nbsp;after the 2010 eruption. Researchers said that this eruption occurred due to combined effect of hot volcanic gases and abundant rainfall.</span>', '2018-07-22 15:10:39', NULL, '5b64310d36608.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

DROP TABLE IF EXISTS `image`;
CREATE TABLE IF NOT EXISTS `image` (
  `img_id` int(10) NOT NULL AUTO_INCREMENT,
  `img_name` varchar(255) NOT NULL,
  `dest_id` int(10) DEFAULT NULL,
  `travel_id` int(10) DEFAULT NULL,
  `slideshow` int(2) DEFAULT NULL,
  `img_cover` int(2) DEFAULT NULL,
  PRIMARY KEY (`img_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`img_id`, `img_name`, `dest_id`, `travel_id`, `slideshow`, `img_cover`) VALUES
(3, '5b51f68495f8c.jpg', 9, NULL, 1, NULL),
(4, '5b51f6849660d.jpg', 9, NULL, 1, 1),
(5, '5b546b38a97af.jpg', 10, NULL, NULL, 1),
(7, '5b549ca30c0b5.jpg', 12, NULL, 1, 1),
(9, '5b64310d13b64.jpg', 13, NULL, 1, NULL),
(10, '5b64310d36608.jpg', 13, NULL, 1, 1),
(11, '5b64310d44025.jpg', 13, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `paket_tour`
--

DROP TABLE IF EXISTS `paket_tour`;
CREATE TABLE IF NOT EXISTS `paket_tour` (
  `id_paket` int(11) NOT NULL AUTO_INCREMENT,
  `paket_name` varchar(100) NOT NULL,
  `highlight` text NOT NULL,
  `itenaery` text NOT NULL,
  `facilities` text NOT NULL,
  `dollar_price` varchar(20) NOT NULL,
  `idr_price` varchar(20) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`id_paket`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paket_tour`
--

INSERT INTO `paket_tour` (`id_paket`, `paket_name`, `highlight`, `itenaery`, `facilities`, `dollar_price`, `idr_price`, `date_added`) VALUES
(1, 'paket 1', 'Paket&nbsp;one day&nbsp;tour&nbsp;ini kami sediakan bagi sahabat Alodia yang hanya memiliki sedikit waktu, namun tetap ingin merasakan serunya destinasi terbaik di Yogyakarta. Paket ini memberikan pengalaman luar biasa buat para sahabat Alodia. Dalam paket ini, sahabat Alodia akan diajak menjelajahi kawasan lereng merapi dengan mobil jeep beratap terbuka, kemudian sahabat Alodia akan diajak mengunjungi salah satu museum terbaik di Yogyakarta yaitu Museum Ullen Sentalu. Setelah itu sahabat Alodia akan diajak mengunjungi Merapi Park yaitu taman yang sengaja dibuat untuk spot foto dengan bangunan-bangunan landmark dari beberapa negara.<div><br></div>', '<ul><li>07.30 - 08.00 Penjemputan di meeting point (Bandara/ Stasiun/ Hotel)</li><li>08.00 - 09.30 Perjalanan menuju basecamp Merapi Lava Tour</li><li>09.30 - 11.30 Merapi Jeep Lava Tour</li><li>11.30 - 12.00 Perjalanan Menuju Restoran Lokal</li><li>12.00 - 13.00 Makan Siang di Restoran Lokal</li><li>13.00 - 13.30 Perjalanan menuju Museum Ullen Sentalu</li><li>13.30 - 14.30 Explore Museum Ullen Sentalu</li><li>14.30 - 15.00 Perjalanan menuju Merapi Park</li><li>15.00 - 16.30 Explore Merapi Park</li><li>16.30 - 18.00 Perjalanan menuju meeting point</li><li>18.00 Program berakhir, sampai jumpa pada perjalanan berikutnya bersama Alodia Tour Yogyakarta</li></ul>', '<div><h2>Include</h2><ul><li>Kendaraan AC standar pariwisata</li><li>Driver as Guide</li><li>BBM</li><li>Parkir</li><li>Drop in dan drop off (Terminal/ Bandara/ Stasiun/ Hotel) di Yogyakarta</li><li>Tiket masuk destinasi sesuai jadwal</li><li>Air mineral</li><li>Jeep Lava Tour</li></ul><div></div></div><div><h2>Exclude</h2><ul><li>Tiket menuju Yogyakarta</li><li>Pengeluaran pribadi</li><li>Tour diluar program</li><li>Biaya tambahan untuk periode&nbsp;high season&nbsp;dan&nbsp;peak season</li></ul></div>', '', '', '2018-08-03 04:20:08'),
(2, 'paket 1', 'Paket&nbsp;one day&nbsp;tour&nbsp;ini kami sediakan bagi sahabat Alodia yang hanya memiliki sedikit waktu, namun tetap ingin merasakan serunya destinasi terbaik di Yogyakarta. Paket ini memberikan pengalaman luar biasa buat para sahabat Alodia. Dalam paket ini, sahabat Alodia akan diajak menjelajahi kawasan lereng merapi dengan mobil jeep beratap terbuka, kemudian sahabat Alodia akan diajak mengunjungi salah satu museum terbaik di Yogyakarta yaitu Museum Ullen Sentalu. Setelah itu sahabat Alodia akan diajak mengunjungi Merapi Park yaitu taman yang sengaja dibuat untuk spot foto dengan bangunan-bangunan landmark dari beberapa negara.<div><br></div>', '<ul><li>07.30 - 08.00 Penjemputan di meeting point (Bandara/ Stasiun/ Hotel)</li><li>08.00 - 09.30 Perjalanan menuju basecamp Merapi Lava Tour</li><li>09.30 - 11.30 Merapi Jeep Lava Tour</li><li>11.30 - 12.00 Perjalanan Menuju Restoran Lokal</li><li>12.00 - 13.00 Makan Siang di Restoran Lokal</li><li>13.00 - 13.30 Perjalanan menuju Museum Ullen Sentalu</li><li>13.30 - 14.30 Explore Museum Ullen Sentalu</li><li>14.30 - 15.00 Perjalanan menuju Merapi Park</li><li>15.00 - 16.30 Explore Merapi Park</li><li>16.30 - 18.00 Perjalanan menuju meeting point</li><li>18.00 Program berakhir, sampai jumpa pada perjalanan berikutnya bersama Alodia Tour Yogyakarta</li></ul>', '<div><h2>Include</h2><ul><li>Kendaraan AC standar pariwisata</li><li>Driver as Guide</li><li>BBM</li><li>Parkir</li><li>Drop in dan drop off (Terminal/ Bandara/ Stasiun/ Hotel) di Yogyakarta</li><li>Tiket masuk destinasi sesuai jadwal</li><li>Air mineral</li><li>Jeep Lava Tour</li></ul><div></div></div><div><h2>Exclude</h2><ul><li>Tiket menuju Yogyakarta</li><li>Pengeluaran pribadi</li><li>Tour diluar program</li><li>Biaya tambahan untuk periode&nbsp;high season&nbsp;dan&nbsp;peak season</li></ul></div>', '', '', '2018-08-03 04:21:03');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

DROP TABLE IF EXISTS `setting`;
CREATE TABLE IF NOT EXISTS `setting` (
  `id` int(2) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `web_name` varchar(200) NOT NULL,
  `web_desc` text NOT NULL,
  `web_corp` varchar(255) NOT NULL,
  `web_corp_address` varchar(200) NOT NULL,
  `web_WA` varchar(20) NOT NULL,
  `web_phone` varchar(20) NOT NULL,
  `web_email` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `username`, `password`, `web_name`, `web_desc`, `web_corp`, `web_corp_address`, `web_WA`, `web_phone`, `web_email`) VALUES
(1, 'huwahu_admin', '0192023a7bbd73250516f069df18b500', 'huwahuholidayjogja', 'Ini adalah deskripsi yang seharusnya sampai berparagraf-paragraf tapi cuma satu kalimat.', 'Hawahu Holidays Jogja Corp.', 'Jl. Sudirman no.xx Kecamatan, Kabupaten, Provinsi', '', '0823135245658', 'mail@example.mail.com');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
