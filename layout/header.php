<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="Content-Type" content="text/html" charset="utf-8">
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Core CSS -->
	<link rel="stylesheet" type="text/css" href="assets/css/style2.css">

      <!-- Custom -->
      <link rel="stylesheet" type="text/css" href="assets/css/custom.css">
      <!-- custom -->

      <!-- Font Awesome CSS-->
      <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">

      <!-- Owl Carousel -->
      <link rel="stylesheet" href="assets/owl-carousel/owl.carousel.css"/>
      <link rel="stylesheet" href="assets/owl-carousel/owl.theme.css"/>
      <!-- Owl Carousel -->

      <!-- slitslider -->
      <link rel="stylesheet" type="text/css" href="assets/slitslider/css/style.css" />
      <link rel="stylesheet" type="text/css" href="assets/slitslider/css/custom.css" />
      <!-- slitslider -->
</head>
<body>
      <!-- HEADER
      ================================================= -->