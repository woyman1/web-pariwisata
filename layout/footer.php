	<!-- FOOTER
	================================================= -->
	<footer id="footer" class="page-footer">
	    <div class="container-fluid clearfix text-center  font-small full-width">
	        <div class="row ">
	            <!--First column-->
	            <div class="footer-partners col-xs-12 col-sm-4 col-md-4 col-lg-4">
	            	<h4><b>Our Partners</b></h4>
	            	<div class="row partners">
	            		<div class="col logo-partner">
	            			<a href="#" ><img src="assets/img/just.jpg" alt="partner"></a>
	            		</div>
	            		<div class=" col logo-partner">
	            			<a href="#" ><img src="assets/img/just.jpg" alt="partner"></a>
	            		</div>
	            		<div class="col logo-partner">
	            			<a href="#" ><img src="assets/img/just.jpg" alt="partner"></a>
	            		</div>
	            		<div class="col logo-partner">
	            			<a href="#" ><img src="assets/img/just.jpg" alt="partner"></a>
	            		</div>            		
		            </div>
	            </div>
	            <!--/.First column-->
 
	            <!--/.Second column-->
                <div class="col-lg-4 col-sm-4">
                      <h4><b>Follow us</b></h4>
                      <a href="https://facebook.com/sewadrone"><img src="assets/img/facebook.png" alt="facebook"></a>
                      <a href="https://twitter.com/sewadrone"><img src="assets/img/twitter.png" alt="twitter"></a>
                      <a href="#"><img src="assets/img/linkedin.png" alt="linkedin"></a>
                      <a href="https://instagram.com/sewadrone"><img src="assets/img/instagram.png" alt="instagram"></a>
                </div>
<?php 
	$qSettingContact = "SELECT * FROM setting WHERE id = '1' ";
	$getSettingContact = mysqli_query($connect, $qSettingContact);
	$contact = mysqli_fetch_assoc($getSettingContact);
 ?> 
                <div class="col-lg-4 col-sm-4">
                    <h4><b>Contact us</b></h4>
                    <p><b><?= $contact['web_corp']; ?></b><br>
                    <span class="glyphicon glyphicon-map-marker"></span> <?= $contact['web_corp_address'] ?> <br>
                    <span class="glyphicon glyphicon-envelope"></span> <?= $contact['web_email']; ?><br>
                    <span class="glyphicon glyphicon-earphone"></span> <?= $contact['web_phone'] ?> </p>
                </div>
	        </div>
	    </div>

	    <!-- Copyright-->
	    <div class="footer-copyright py-3 text-center full-width">
	    	<smal>© 2018 Copyright:<a href="#"><?= $contact['web_name'] ?></a></smal>
	    </div>
	    <!--/.Copyright -->
	</footer>
	<!-- End FOOTER -->
