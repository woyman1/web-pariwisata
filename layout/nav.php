<header id="navbar">	

	<?php 
	$qSettingContact = "SELECT * FROM setting WHERE id = '1' ";
	$getSettingContact = mysqli_query($connect, $qSettingContact);
	$contact = mysqli_fetch_assoc($getSettingContact);
 ?> 

	<a href="home" class="navbar-brand" style="float: left">
		<img src="assets/img/logoUtama.png" alt="home" width="300" /> 
	</a> 

			<div class="container navbar navbar-light navbar-toggleable-sm navbar-expand-sm d-flex">
				<div class="navbar-wrapper" >

					<div id="navbar-header">
						<!-- Logo -->

					</div>
					<div class="col-md-3 row" style="float: right; height:10px; display: inline-flex; margin-left: 50px;">			
						<div class="col-md-12" style="float: right; margin-right: 0px;">
							<h6 class="col-md-12" style="margin-bottom: 1px; text-align: center;"><b>Contact us</b></h6>
							<p style="margin-top: 1px; font-size: 12px; text-align: center;">
								Email: <?= $contact['web_email']; ?><br>
                    			WA/Phone: </span> <?= $contact['web_phone'] ?> 
                    		</p>
                    	</div>
					</div>
					<!-- Menu Navigation -->
					<div id="navbar-menu" class="collapse navbar-collapse">
						<ul class="nav navbar-nav top-menu navbar-right">
							<li class="menu"><a href="home" class="link-menu">Home<span class="top-menu-active"></span></a></li>
							<li class="menu"><a href="destination" class="link-menu">Destination<span class="top-menu-active"></span></a></li>
							<li class="menu"><a href="packagesList" class="link-menu">Travel Package<span class="top-menu-active"></span></a></li>
							<!-- <li class="menu right-book"><a class="link-menu" onclick="openbookingpage()">Booking<span class="top-menu-active"></span></a></li> -->
							<!--  Language User -->
							<!-- <li class="menu top-language"><div id="google_translate_element"></div></li> -->
							<!-- END Language User -->
						</ul>
					</div>



					</div>
					<!-- End Menu -->
				</div>
			</div>
			<script type="text/javascript">
				function googleTranslateElementInit() {
					new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
				}
		        function openbookingpage(){
		            //$(".language-shown-hide").show();
		            $(".booking-page").show();
		            $(".booking-shown-hide").css({"opacity":"1","overflow": "unset"});
		            setTimeout(function(){ 
		                navEl.hide();
		            }, 500);
		        }
		        function closebookingpage(){
		            $(".booking-shown-hide").css({"opacity":"0","overflow": "hidden"})
		            $(".booking-page").hide();
		        }
			</script>
			<div class="booking-shown-hide" >
				<div class="booking-page" style="display: none;"> </div>
				<div class="full-width" id="booking-page" style="z-index: 16;position: relative;">
					<div class="container">
				    	<div class="row" data-spy="scroll" data-offset="0">
				            <div class="tour-booking-form" style="margin-top: 50px;">
				                    <form>
				                        <div class="row">
				                            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
				                                <div class="form-group">
				                                    <label class="control-label required" for="select">Destination</label>
				                                    <div class="select">
				                                        <select id="select" name="select" class="form-control">
				                                            <option value="">Where you want prefer to go</option>
				                                            <option value="">Destination 1</option>
				                                            <option value="">Destination 2</option>
				                                            <option value="">Destination 3</option>
				                                        </select>
				                                    </div>
				                                </div>
				                            </div>
				                            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
				                            <div class="form-group">
				                                <label class="control-label" for="datepicker">Check in</label>
				                                <div class="input-group">
				                                    <input id="datepicker" name="datepicker" type="text" placeholder="Date" class="form-control" required>
				                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span> </div>
				                                </div>
				                            </div>
				                            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
				                                <div class="form-group">
				                                    <label class="control-label required" for="select">Number of Persons:</label>
				                                    <div class="select">
				                                        <select id="select" name="select" class="form-control">
				                                            <option value="">Number of Persons:</option>
				                                            <option value="">01</option>
				                                            <option value="">02</option>
				                                            <option value="">03</option>
				                                        </select>
				                                    </div>
				                                </div>
				                            </div>
				                            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
				                               <div class="form-group">
				                                    <label class="control-label required" for="packages">Travel Packages</label>
				                                    <div class="select">
				                                        <select id="packages" name="packages" class="form-control">
				                                            <option value="">Select your packages</option>
				                                            <option value="">Packages 1</option>
				                                            <option value="">Packages 2</option>
				                                            <option value="">Packages 3</option>
				                                            <option value="">Packages 4</option>
				                                            <option value="">Packages 5</option>
				                                            <option value="">Packages 6</option>
				                                            <option value="">Packages 7</option>
				                                            <option value="">Packages 8</option>
				                                        </select>
				                                    </div>
				                                </div>
				                            </div>
				                            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
				                                <div class="form-group">
				                                    <label class="control-label" for="name">Name</label>
				                                    <input id="name" type="text" placeholder="First Name" class="form-control" required>
				                                </div>
				                            </div>
				                            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
				                                <div class="form-group">
				                                    <label class="control-label" for="email"> Email</label>
				                                    <input id="email" type="text" placeholder="yourmail@mail.com" class="form-control" required>
				                                </div>
				                            </div>
				                            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
				                                <div class="form-group">
				                                    <label class="control-label" for="phone"> Phone</label>
				                                    <input id="phone" type="text" placeholder="(222) 222-2222" class="form-control" required>
				                                </div>
				                            </div>
				                            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
				                                <div class="form-group">
				                                    <label class="control-label" for="country">Country</label>
				                                    <input id="country" type="text" placeholder="Your country" class="form-control" required>
				                                </div>
				                            </div>
				                            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
				                                <div class="form-group">
				                                    <label class="control-label" for="city">City</label>
				                                    <input id="city" type="text" placeholder="Your city" class="form-control" required>
				                                </div>
				                            </div>
				                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				                                <div class="form-group">
				                                    <label class="control-label" for="textarea">Describe Your Travel Requirements</label>
				                                    <textarea class="form-control" id="textarea" name="textarea" rows="4" placeholder="Write Your Requirements"></textarea>
				                                </div>
				                            </div>
				                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				                                <button type="submit" name="booking" class="btn btn-primary">send Enquiry</button>
				                                <button type="reset" name="cancel" class="btn btn-default btn-cancel" onclick="closebookingpage()">Cancel</button>
				                            </div>
				                        </div>
				                    </form>
				            </div>
				        </div>
					</div>
				</div>
			</div>
	</header>
	<!-- END HEADER -->